module functions
  !! This module contains several common geometry-related functions
  !! used by the program, namely distances, angles and dihedrals.
  use precision, only : dp

  implicit none
  public :: dist
  public :: dist_v2
  public :: dist2_v2
  public :: angle_v2
  public :: scalar_v2
  public :: scalar_vec
  public :: vec_norm
  public :: vec_norm2
  public :: dihedro_v2
  public :: dihedro2_v2
  public :: diheforce_v2
  public :: diheforce2_v2

  private
  real(dp), parameter :: TINY_VAL = 1.0e-14_dp
    !! Used when comparing a result to zero.

contains

  function dist_pbc( r1, r2, r_size ) result ( dr )
    !! Calculates the distance along an axis between to points,
    !! taking into account periodic boundary conditions.
    implicit none
    real(dp), intent(in) :: r1
      !! Coordinate for the first point along the axis.
    real(dp), intent(in) :: r2
      !! Coordinate for the second point along the axis.
    real(dp), intent(in) :: r_size
      !! Size of the cell along the axis.

    real(dp) :: dr

    dr = r1 - r2
    dr = dr - ANINT( dr / r_size ) * r_size
  end function dist_pbc

  function dist( x1, y1, z1, x2, y2, z2, cell ) result( dist_t )
    !! Calculates the distance between two points taking into account
    !! periodic boundary conditions.
    use precision, only : dp

    implicit none
    real(dp), intent(in) :: x1
      !! X coordinate for the first point.
    real(dp), intent(in) :: y1
      !! Y coordinate for the first point.
    real(dp), intent(in) :: z1
      !! Z coordinate for the first point.
    real(dp), intent(in) :: x2
      !! X coordinate for the second point.
    real(dp), intent(in) :: y2
      !! Y coordinate for the second point.
    real(dp), intent(in) :: z2
      !! Z coordinate for the second point.
    real(dp), intent(in) :: cell(3,3)
      !! Periodic cell vectors.

    real(dp) :: dx, dy, dz, dist_t

    dx = dist_pbc( x1, x2, cell(1,1) )
    dy = dist_pbc( y1, y2, cell(2,2) )
    dz = dist_pbc( z1, z2, cell(3,3) )

    dist_t = dist_v2( dx, dy, dz )
  end function dist

  function dist_v2( dx, dy, dz ) result( dv2 )
    !! Calculates the norm of a distance vector.
    use precision, only : dp
    implicit none
    real(dp) :: dx
      !! Distance along the X axis.
    real(dp) :: dy
      !! Distance along the Y axis.
    real(dp) :: dz
      !! Distance along the Z axis.
    real(dp) :: dv2

    dv2 = dx * dx + dy * dy + dz * dz
    dv2 = sqrt( dv2 )
  end function dist_v2

  function dist2_v2( dx, dy, dz ) result( dv2 )
    !! Calculates the squared norm of a distance vector.
    use precision, only : dp

    implicit none
    real(dp) :: dx
      !! Distance along the X axis.
    real(dp) :: dy
      !! Distance along the Y axis.
    real(dp) :: dz
      !! Distance along the Z axis.
    real(dp) :: dv2

    dv2 = dx * dx + dy * dy + dz * dz
  end function dist2_v2

  function angle_v2( dx12, dy12, dz12, dx32, dy32, dz32 ) result( angle_t )
    !! Calculates the angle between two vectors sharing a point.
    use precision, only : dp
    implicit none

    real(dp) :: dx12
      !! Distance along the X axis for vector 1.
    real(dp) :: dy12
      !! Distance along the Y axis for vector 1.
    real(dp) :: dz12
      !! Distance along the Z axis for vector 1.
    real(dp) :: dx32
      !! Distance along the X axis for vector 2.
    real(dp) :: dy32
      !! Distance along the Y axis for vector 2.
    real(dp) :: dz32
      !! Distance along the Z axis for vector 2.
    real(dp) :: angle_t, scalarp

    scalarp = dx12 * dx32 + dy12 * dy32 + dz12 * dz32
    angle_t = dist_v2( dx12, dy12, dz12 ) * dist_v2( dx32, dy32, dz32 )

    angle_t = scalarp / angle_t
    if ( angle_t >  1.0_dp ) angle_t =  1.0_dp
    if ( angle_t < -1.0_dp ) angle_t = -1.0_dp

    angle_t = ACOS( angle_t ) * 180.0_dp / ACOS( -1.0_dp )
  end function angle_v2

  function scalar_v2( x1, y1, z1, x2, y2, z2 ) result( scalar_t )
    !! Calculates the scalar product between two vectors.
    use precision, only : dp
    implicit none

    real(dp), intent(in) :: x1
      !! X length for the first vector.
    real(dp), intent(in) :: y1
      !! Y length for the first vector.
    real(dp), intent(in) :: z1
      !! Z length for the first vector.
    real(dp), intent(in) :: x2
      !! X length for the second vector.
    real(dp), intent(in) :: y2
      !! Y length for the second vector.
    real(dp), intent(in) :: z2
      !! Z length for the second vector.

    real(dp) :: scalar_t
    scalar_t = x1 * x2 + y1 * y2 + z1 * z2
  end function scalar_v2

  function scalar_vec( v1, v2, vsize ) result( scalar_t )
    !! Calculates the scalar product between two vectors of any dimension.
    use precision, only : dp
    implicit none
    integer , intent(in) :: vsize
      !! Vector sizes of v1 and v2.
    real(dp), intent(in) :: v1(vsize)
      !! The first vector.
    real(dp), intent(in) :: v2(vsize)
      !! The second vector.

    integer  :: iCrd
    real(dp) :: scalar_t

    scalar_t = 0.0_dp
    do iCrd = 1, vsize
      scalar_t = scalar_t + v1(iCrd) * v2(iCrd)
    enddo
  end function scalar_vec

  function vec_norm( v1, vsize ) result( vnorm )
    !! Calculates the cartesian norm of a vector.
    use precision, only : dp
    implicit none
    integer , intent(in) :: vsize
      !! Vector sizes of v1 and v2.
    real(dp), intent(in) :: v1(vsize)
      !! The vector.

    real(dp) :: vnorm

    vnorm = sqrt( scalar_vec( v1, v1, vsize ) )
  end function vec_norm

  function vec_norm2( v1, vsize ) result( vnorm )
    !! Calculates the squared cartesian norm of a vector.
    use precision, only : dp
    implicit none
    integer , intent(in) :: vsize
      !! Vector sizes of v1 and v2.
    real(dp), intent(in) :: v1(vsize)
      !! The vector.

    real(dp) :: vnorm

    vnorm = scalar_vec( v1, v1, vsize )
  end function vec_norm2

  function dihedro_v2( x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, &
                       cell, kcell, lattice_type ) result( dihe_t )
    !! Calculates the dihedral angle given by four points, taking into
    !! account periodic boundary conditions.
    use precision, only : dp

    implicit none
    real(dp)        , intent(in) :: x1
      !! X coordinate for the first point.
    real(dp)        , intent(in) :: y1
      !! Y coordinate for the first point.
    real(dp)        , intent(in) :: z1
      !! Z coordinate for the first point.
    real(dp)        , intent(in) :: x2
      !! X coordinate for the second point.
    real(dp)        , intent(in) :: y2
      !! Y coordinate for the second point.
    real(dp)        , intent(in) :: z2
      !! Z coordinate for the second point.
    real(dp)        , intent(in) :: x3
      !! X coordinate for the third point.
    real(dp)        , intent(in) :: y3
      !! Y coordinate for the third point.
    real(dp)        , intent(in) :: z3
      !! Z coordinate for the third point.
    real(dp)        , intent(in) :: x4
      !! X coordinate for the fourth point.
    real(dp)        , intent(in) :: y4
      !! Y coordinate for the fourth point.
    real(dp)        , intent(in) :: z4
      !! Z coordinate for the fourth point.
    real(dp)        , intent(in) :: cell(3,3)
      !! Periodic cell vectors.
    real(dp)        , intent(in) :: kcell(3,3)
      !! Periodic cell vectors in the reciprocal space, without 2PI.
    character(len=1), intent(in) :: lattice_type
      !! Type of lattice used.

    real(dp) :: dihe_t
    real(dp) :: rm, rn, nx, ny, nz, mx, my, mz

    dihe_t = 0.0_dp

    ! The following routine returns the dihedral among other variables.
    ! The results of n*, and m* are not relevant here.
    call dihevars_v2( x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, &
                      mx, my, mz, rm, nx, ny, nz, rn, dihe_t, cell,   &
                      kcell, lattice_type )

  end function dihedro_v2

  function dihedro2_v2( x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, &
                        cell, kcell, lattice_type ) result( dihe_t )
    !! Calculates the dihedral angle given by four points, taking into
    !! account periodic boundary conditions and additional corrections.
    use precision, only : dp
    use qmmm_pbc , only : pbc_displ_vector

    implicit none
    real(dp)        , intent(in) :: x1
      !! X coordinate for the first point.
    real(dp)        , intent(in) :: y1
      !! Y coordinate for the first point.
    real(dp)        , intent(in) :: z1
      !! Z coordinate for the first point.
    real(dp)        , intent(in) :: x2
      !! X coordinate for the second point.
    real(dp)        , intent(in) :: y2
      !! Y coordinate for the second point.
    real(dp)        , intent(in) :: z2
      !! Z coordinate for the second point.
    real(dp)        , intent(in) :: x3
      !! X coordinate for the third point.
    real(dp)        , intent(in) :: y3
      !! Y coordinate for the third point.
    real(dp)        , intent(in) :: z3
      !! Z coordinate for the third point.
    real(dp)        , intent(in) :: x4
      !! X coordinate for the fourth point.
    real(dp)        , intent(in) :: y4
      !! Y coordinate for the fourth point.
    real(dp)        , intent(in) :: z4
      !! Z coordinate for the fourth point.
    real(dp)        , intent(in) :: cell(3,3)
      !! Periodic cell vectors.
    real(dp)        , intent(in) :: kcell(3,3)
      !! Periodic cell vectors in the reciprocal space, without 2PI.
    character(len=1), intent(in) :: lattice_type
      !! Type of lattice used.

    real(dp) :: dihe_t
    real(dp) :: rm, rn, nx, ny, nz, mx, my, mz, planeA, planeB, &
                planeC, l1, l2, dx12, dy12, dz12, dx31, dy31, dz31

    dihe_t = 0.0_dp

    ! The following routine returns the dihedral among other variables.
    ! The results of n*, and m* are not relevant here.
    call dihevars_v2( x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, &
                      mx, my, mz, rm, nx, ny, nz, rn, dihe_t, cell,   &
                      kcell, lattice_type )

    ! Which cuadrant are we in?
    dx12 = x1 - x2
    dy12 = y1 - y2
    dz12 = z1 - z2
    call pbc_displ_vector( lattice_type, cell, kcell, dx12, dy12, dz12 )

    dx31 = x3 - x1
    dy31 = y3 - y1
    dz31 = z3 - z1
    call pbc_displ_vector( lattice_type, cell, kcell, dx31, dy31, dz31 )

    planeA = -dy12 * dz31 + dz12 * dy31
    planeB = -dz12 * dx31 + dx12 * dz31
    planeC = -dx12 * dy31 + dy12 * dx31

    ! Distance(l) from atom4 to the ABCD = 0 plane
    l1 = planeA * x4 + planeB * y4 + planeC * z4 &
       - planeA * x1 - planeB * y1 - planeC * z1
    l2 = sqrt( planeA * planeA + planeB * planeB + planeC * planeC )

    if ( ( l1 / l2 ) < 0.0_dp ) dihe_t = 360.0_dp - dihe_t
  end function dihedro2_v2

  subroutine diheforce_v2( nac, ramber, i1, i2, i3, i4, atom, kd, eqd, per, &
                           mult, fce, cell, kcell, lattice_type )
    !! Calculates the force over a dihedral angle using the AMBER force field.
    use precision, only : dp
    use qmmm_pbc , only : pbc_displ_vector

    implicit none
    integer         , intent(in)  :: nac
      !! Number of atoms.
    real(dp)        , intent(in)  :: ramber(3,nac)
      !! Atomic positions.
    integer         , intent(in)  :: i1
      !! Index of atom n°1 conforming the dihedral.
    integer         , intent(in)  :: i2
      !! Index of atom n°2 conforming the dihedral.
    integer         , intent(in)  :: i3
      !! Index of atom n°3 conforming the dihedral.
    integer         , intent(in)  :: i4
      !! Index of atom n°4 conforming the dihedral.
    integer         , intent(in)  :: atom
      !! Atom of the dihedral we are interested in for the derivative.
      !! Goes from 1 to 4.
    real(dp)        , intent(in)  :: kd
      !! Equilibrium constant of current dihedral.
    real(dp)        , intent(in)  :: eqd
      !! Equilibrium value (d0) of the dihedral.
    real(dp)        , intent(in)  :: per
      !! Periodicity of the dihedral.
    integer         , intent(in)  :: mult
      !! Multiplicity (i.e. number of periods in 360°) of current dihedral.
    real(dp)        , intent(in)  :: cell(3,3)
      !! Cell vectors.
    real(dp)        , intent(in) :: kcell(3,3)
      !! Periodic cell vectors in the reciprocal space, without 2PI.
    character(len=1), intent(in) :: lattice_type
      !! Type of lattice used.
    real(dp)        , intent(out) :: fce(12)
      !! Forces for all atoms in the dihedral in the following order:
      !! at1x, at1y, at1z, at2x, ..., at4z.

    integer  :: iFrc, iCrd
    real(dp) :: dtot, rm, rn, mx, my, mz, nx, ny, nz, scal, dscalar, &
                dm, dn, dmx, dmy, dmz, dnx, dny, dnz, dmn, prue, dih

    call dihevars_v2( ramber(1,i1), ramber(2,i1), ramber(3,i1),  &
                      ramber(1,i2), ramber(2,i2), ramber(3,i2),  &
                      ramber(1,i3), ramber(2,i3), ramber(3,i3),  &
                      ramber(1,i4), ramber(2,i4), ramber(3,i4),  &
                      mx, my, mz, rm, nx, ny, nz, rn, dih, cell, &
                      kcell, lattice_type )
    fce(:) = 0.0_dp
    scal = mx * nx + my * ny + mz * nz

    prue = scal / ( rn * rm )
    prue = ( 1.0_dp - prue * prue )
    if ( abs(prue) < TINY_VAL ) return

    dtot = SIN( ( ACOS(-1.0_dp) / 180.0_dp ) * ( per * dih - eqd ) )
    dtot = ( kd / mult ) * dtot * per / sqrt(prue)

    do iCrd = 1, 3
      iFrc = ( atom -1 ) * 3 + iCrd

      dmx = 0.0_dp ; dmy = 0.0_dp ; dmz = 0.0_dp
      dnx = 0.0_dp ; dny = 0.0_dp ; dnz = 0.0_dp

      !! Here we go jumping in threes due to similitudes between
      !! calculations, i.e. we go 1-4-7-10, then 2-5-8-11, and
      !! finally 3-6-9-12, which would be X for all atoms, then Y
      !! for all atoms, then Z.
      select case( iFrc )
      case ( 1 )
        dmy = ramber(3,i2) - ramber(3,i3)
        dmz = ramber(2,i3) - ramber(2,i2)
      case ( 4 )
        dmy = ramber(3,i3) - ramber(3,i1)
        dmz = ramber(2,i1) - ramber(2,i3)
        dny = ramber(3,i3) - ramber(3,i4)
        dnz = ramber(2,i4) - ramber(2,i3)
      case ( 7 )
        dmy = ramber(3,i1) - ramber(3,i2)
        dmz = ramber(2,i2) - ramber(2,i1)
        dny = ramber(3,i4) - ramber(3,i2)
        dnz = ramber(2,i2) - ramber(2,i4)
      case ( 10 )
        dny = ramber(3,i2) - ramber(3,i3)
        dnz = ramber(2,i3) - ramber(2,i2)
      case ( 2 )
        dmx = ramber(3,i3) - ramber(3,i2)
        dmz = ramber(1,i2) - ramber(1,i3)
      case ( 5 )
        dmx = ramber(3,i1) - ramber(3,i3)
        dmz = ramber(1,i3) - ramber(1,i1)
        dnx = ramber(3,i4) - ramber(3,i3)
        dnz = ramber(1,i3) - ramber(1,i4)
      case ( 8 )
        dmx = ramber(3,i2) - ramber(3,i1)
        dmz = ramber(1,i1) - ramber(1,i2)
        dnx = ramber(3,i2) - ramber(3,i4)
        dnz = ramber(1,i4) - ramber(1,i2)
      case ( 11 )
        dnx = ramber(3,i3) - ramber(3,i2)
        dnz = ramber(1,i2) - ramber(1,i3)
      case ( 3 )
        dmx = ramber(2,i2) - ramber(2,i3)
        dmy = ramber(1,i3) - ramber(1,i2)
      case ( 6 )
        dmx = ramber(2,i3) - ramber(2,i1)
        dmy = ramber(1,i1) - ramber(1,i3)
        dnx = ramber(2,i3) - ramber(2,i4)
        dny = ramber(1,i4) - ramber(1,i3)
      case ( 9 )
        dmx = ramber(2,i1) - ramber(2,i2)
        dmy = ramber(1,i2) - ramber(1,i1)
        dnx = ramber(2,i4) - ramber(2,i2)
        dny = ramber(1,i2) - ramber(1,i4)
      case ( 12 )
        dnx = ramber(2,i2) - ramber(2,i3)
        dny = ramber(1,i3) - ramber(1,i2)
      end select

      call pbc_displ_vector( lattice_type, cell, kcell, dmx, dmy, dmz )
      call pbc_displ_vector( lattice_type, cell, kcell, dnx, dny, dnz )

      dm  = (mx * dmx + my * dmy + mz * dmz) / rm
      dn  = (nx * dnx + ny * dny + nz * dnz) / rn
      dmn = rm * dn + rn * dm

      dscalar = nx * dmx + mx * dnx + ny * dmy + my * dny &
              + nz * dmz + mz * dnz
      fce(iFrc)  = dtot * ( dscalar * rm * rn - dmn * scal ) &
                        / ( rn * rn * rm * rm )
    enddo
  end subroutine diheforce_v2

  subroutine diheforce2_v2( nac, ramber, i1, i2, i3, i4, atom, kd, fce, &
                            cell, kcell, lattice_type )
    !! Calculates the force over a dihedral angle in a more general case.
    use precision, only : dp
    use qmmm_pbc , only : pbc_displ_vector

    implicit none
    integer         , intent(in)  :: nac
      !! Number of atoms.
    real(dp)        , intent(in)  :: ramber(3,nac)
      !! Atomic positions.
    integer         , intent(in)  :: i1
      !! Index of atom n°1 conforming the dihedral.
    integer         , intent(in)  :: i2
      !! Index of atom n°2 conforming the dihedral.
    integer         , intent(in)  :: i3
      !! Index of atom n°3 conforming the dihedral.
    integer         , intent(in)  :: i4
      !! Index of atom n°4 conforming the dihedral.
    integer         , intent(in)  :: atom
      !! Atom of the dihedral we are interested in for the derivative.
      !! Goes from 1 to 4.
    real(dp)        , intent(in)  :: kd
      !! Equilibrium constant of current dihedral.
    real(dp)        , intent(in)  :: cell(3,3)
      !! Cell vectors.
    real(dp)        , intent(in)  :: kcell(3,3)
      !! Periodic cell vectors in the reciprocal space, without 2PI.
    character(len=1), intent(in)  :: lattice_type
      !! Type of lattice used.
    real(dp)        , intent(out) :: fce(12)
      !! Forces for all atoms in the dihedral in the following order:
      !! at1x, at1y, at1z, at2x, ..., at4z.

    integer  :: iFrc, iCrd
    real(dp) :: dtot, rm, rn, mx, my, mz, nx, ny, nz, scal, dscalar, &
                dm, dn, dmx, dmy, dmz, dnx, dny, dnz, dmn, prue, dih

    call dihevars_v2( ramber(1,i1), ramber(2,i1), ramber(3,i1),  &
                      ramber(1,i2), ramber(2,i2), ramber(3,i2),  &
                      ramber(1,i3), ramber(2,i3), ramber(3,i3),  &
                      ramber(1,i4), ramber(2,i4), ramber(3,i4),  &
                      mx, my, mz, rm, nx, ny, nz, rn, dih, cell, &
                      kcell, lattice_type )
    fce(:) = 0.0_dp
    scal = mx * nx + my * ny + mz * nz

    prue = scal / ( rn * rm )
    prue = ( 1.0_dp - prue * prue )
    if ( abs(prue) < TINY_VAL ) return

    dtot = - kd / sqrt(prue)

    do iCrd = 1, 3
      iFrc = ( atom -1 ) * 3 + iCrd

      dmx = 0.0_dp ; dmy = 0.0_dp ; dmz = 0.0_dp
      dnx = 0.0_dp ; dny = 0.0_dp ; dnz = 0.0_dp

      !! Here we go jumping in threes due to similitudes between
      !! calculations, i.e. we go 1-4-7-10, then 2-5-8-11, and
      !! finally 3-6-9-12, which would be X for all atoms, then Y
      !! for all atoms, then Z.
      select case( iFrc )
      case ( 1 )
        dmy = ramber(3,i2) - ramber(3,i3)
        dmz = ramber(2,i3) - ramber(2,i2)
      case ( 4 )
        dmy = ramber(3,i3) - ramber(3,i1)
        dmz = ramber(2,i1) - ramber(2,i3)
        dny = ramber(3,i3) - ramber(3,i4)
        dnz = ramber(2,i4) - ramber(2,i3)
      case ( 7 )
        dmy = ramber(3,i1) - ramber(3,i2)
        dmz = ramber(2,i2) - ramber(2,i1)
        dny = ramber(3,i4) - ramber(3,i2)
        dnz = ramber(2,i2) - ramber(2,i4)
      case ( 10 )
        dny = ramber(3,i2) - ramber(3,i3)
        dnz = ramber(2,i3) - ramber(2,i2)
      case ( 2 )
        dmx = ramber(3,i3) - ramber(3,i2)
        dmz = ramber(1,i2) - ramber(1,i3)
      case ( 5 )
        dmx = ramber(3,i1) - ramber(3,i3)
        dmz = ramber(1,i3) - ramber(1,i1)
        dnx = ramber(3,i4) - ramber(3,i3)
        dnz = ramber(1,i3) - ramber(1,i4)
      case ( 8 )
        dmx = ramber(3,i2) - ramber(3,i1)
        dmz = ramber(1,i1) - ramber(1,i2)
        dnx = ramber(3,i2) - ramber(3,i4)
        dnz = ramber(1,i4) - ramber(1,i2)
      case ( 11 )
        dnx = ramber(3,i3) - ramber(3,i2)
        dnz = ramber(1,i2) - ramber(1,i3)
      case ( 3 )
        dmx = ramber(2,i2) - ramber(2,i3)
        dmy = ramber(1,i3) - ramber(1,i2)
      case ( 6 )
        dmx = ramber(2,i3) - ramber(2,i1)
        dmy = ramber(1,i1) - ramber(1,i3)
        dnx = ramber(2,i3) - ramber(2,i4)
        dny = ramber(1,i4) - ramber(1,i3)
      case ( 9 )
        dmx = ramber(2,i1) - ramber(2,i2)
        dmy = ramber(1,i2) - ramber(1,i1)
        dnx = ramber(2,i4) - ramber(2,i2)
        dny = ramber(1,i2) - ramber(1,i4)
      case ( 12 )
        dnx = ramber(2,i2) - ramber(2,i3)
        dny = ramber(1,i3) - ramber(1,i2)
      end select

      call pbc_displ_vector( lattice_type, cell, kcell, dmx, dmy, dmz )
      call pbc_displ_vector( lattice_type, cell, kcell, dnx, dny, dnz )

      dm  = (mx * dmx + my * dmy + mz * dmz) / rm
      dn  = (nx * dnx + ny * dny + nz * dnz) / rn
      dmn = rm * dn + rn * dm

      dscalar = nx * dmx + mx * dnx + ny * dmy + my * dny &
              + nz * dmz + mz * dnz
      fce(iFrc)  = dtot * ( dscalar * rm * rn - dmn * scal ) &
                        / ( rn * rn * rm * rm )
    enddo
  end subroutine diheforce2_v2

  subroutine dihevars_v2( x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4, &
                          mx, my, mz, rm, nx, ny, nz, rn, dihe_t, cell,   &
                          kcell, lattice_type )
    !! Calculates the variables related to a dihedral angle.
    use precision, only : dp
    use qmmm_pbc , only : pbc_displ_vector
    use sys      , only : die

    implicit none
    real(dp)        , intent(in) :: x1
      !! X coordinate for the first point.
    real(dp)        , intent(in) :: y1
      !! Y coordinate for the first point.
    real(dp)        , intent(in) :: z1
      !! Z coordinate for the first point.
    real(dp)        , intent(in) :: x2
      !! X coordinate for the second point.
    real(dp)        , intent(in) :: y2
      !! Y coordinate for the second point.
    real(dp)        , intent(in) :: z2
      !! Z coordinate for the second point.
    real(dp)        , intent(in) :: x3
      !! X coordinate for the third point.
    real(dp)        , intent(in) :: y3
      !! Y coordinate for the third point.
    real(dp)        , intent(in) :: z3
      !! Z coordinate for the third point.
    real(dp)        , intent(in) :: x4
      !! X coordinate for the fourth point.
    real(dp)        , intent(in) :: y4
      !! Y coordinate for the fourth point.
    real(dp)        , intent(in) :: z4
      !! Z coordinate for the fourth point.
    real(dp)        , intent(in) :: cell(3,3)
      !! Periodic cell vectors.
    real(dp)        , intent(in) :: kcell(3,3)
      !! Periodic cell vectors in the reciprocal space, without 2PI.
    character(len=1), intent(in) :: lattice_type
      !! Type of lattice used.
    real(dp), intent(out) :: mx
      !! X coordinate for output vector M.
    real(dp), intent(out) :: my
      !! Y coordinate for output vector M.
    real(dp), intent(out) :: mz
      !! Z coordinate for output vector M.
    real(dp), intent(out) :: rm
      !! Cartesian norm of output vector M.
    real(dp), intent(out) :: nx
      !! X coordinate for output vector N.
    real(dp), intent(out) :: ny
      !! Y coordinate for output vector N.
    real(dp), intent(out) :: nz
      !! Z coordinate for output vector N.
    real(dp), intent(out) :: rn
      !! Cartesian norm of output vector N.
    real(dp), intent(out) :: dihe_t
      !! Value of the dihedral angle.

    real(dp) :: dx12, dy12, dz12, dx32, dy32, dz32, &
                dx43, dy43, dz43, scalarp, cos_alpha

    dihe_t = 0.0_dp

    dx12 = x1 - x2
    dy12 = y1 - y2
    dz12 = z1 - z2
    call pbc_displ_vector( lattice_type, cell, kcell, dx12, dy12, dz12 )
    dx32 = x3 - x2
    dy32 = y3 - y2
    dz32 = z3 - z2
    call pbc_displ_vector( lattice_type, cell, kcell, dx32, dy32, dz32 )
    dx43 = x4 - x3
    dy43 = y4 - y3
    dz43 = z4 - z3
    call pbc_displ_vector( lattice_type, cell, kcell, dx43, dy43, dz43 )

    mx = dy12 * dz32 - dz12 * dy32
    my = dz12 * dx32 - dx12 * dz32
    mz = dx12 * dy32 - dy12 * dx32

    nx = dz32 * dy43 - dy32 * dz43
    ny = dx32 * dz43 - dz32 * dx43
    nz = dy32 * dx43 - dx32 * dy43

    ! Scalar product of n * m and their norms.
    scalarp = mx * nx + my * ny + mz * nz
    rm  = sqrt( mx * mx + my * my + mz * mz )
    rn  = sqrt( nx * nx + ny * ny + nz * nz )

    cos_alpha = scalarp / ( rm * rn )
    if ( abs( cos_alpha ) > 1.00001_dp ) &
      call die( 'qmmm dihevars_v2: abs(cos_alpha) > 1.00001' )

    if ( cos_alpha >  1.0_dp ) cos_alpha =  1.0_dp
    if ( cos_alpha < -1.0_dp ) cos_alpha = -1.0_dp

    dihe_t = ACOS( cos_alpha ) * 180.0_dp / ACOS(-1.0_dp)
  end subroutine dihevars_v2
end module functions
