
module m_qmmm_fdf
  public :: fdf_block_qmmm
  public :: chrlen_qmmm
  public :: fdf_shutdown
  public :: leqi

  private
  ! Copyright Alberto Garcia, Jose Soler, 1996, 1997, 1998
  !     I/O variables for the fdf package.
  !     ndepth: number of open files (maximum maxdepth)
  !     fdf_stack holds their unit numbers.

  integer, parameter :: maxdepth = 5
    !! Maximum number of stacked depth, i.e. when using includes.
  integer :: ndepth
    !! Current include depth.
  integer :: fdf_stack(maxdepth)
    !! Unit numbers of opened fdf files with includes.

  ! Unit numbers for input, output, error notification, and
  ! debugging output (the latter active if fdf_debug is true).

  integer, save :: fdf_in
    !! File unit for fdf input.
  integer, save :: fdf_out
    !! File unit for fdf output.
  integer, save :: fdf_err
    !! File unit for fdf error notifications.
  integer, save :: fdf_log
    !! File unit for fdf debug log (if fdf_debug is true).
  logical, save :: fdf_debug = .false.
    !! Whether we output debug information.
  logical, save :: fdf_debug2  = .false.
    !! Second level of debug information, for greater pleasure.
  logical, save :: fdf_started = .false.
    !! Whether the fdf module is initialized.
  logical, save :: fdf_donothing = .false.
    !! Only for debugging purposes.

  character(len=132) :: line
    !! Current line being read and parsed.
  integer, parameter :: maxntokens = 50
    !! Max number of tokens in line.

  integer :: ntokens
    !! Current number of tokens in line.
  integer :: first(maxntokens)
    !! First usable token in line.
  integer :: last(maxntokens)
    !! Last usable token in line.
contains

  subroutine qmmm_fdf_init( )
    !! New initialization for fdf. Simplified user interface using the io
    !! package.
    implicit none
    integer           :: debug_level
    character(len=20) :: filein

    if ( fdf_donothing ) return

    ! Prevent the user from opening two head files
    if ( fdf_started ) then
      write( fdf_err, '(a)' ) 'FDF: Head file already set...'
      stop 'HEAD'
    endif

    call io_geterr( fdf_err )
    ndepth = 0

    call io_assign( fdf_out )
    open( unit = fdf_out, file = 'qmmm_fdf.log', form = 'formatted', &
          status = 'unknown' )
    rewind( fdf_out )

    filein = 'qmmm_block_input'
    call fdf_open( filein )
    write( fdf_out,'(/,a,a,a,i3,/)' ) '#FDF: Opened ', filein, &
                                      ' for input. Unit:', fdf_in

    fdf_started = .true.

    debug_level = fdf_integer( 'fdf-debug', 0 )
    call fdf_setdebug( debug_level )

  end subroutine qmmm_fdf_init

  subroutine fdf_shutdown
    !! Closes the fdf 'head' file.
    implicit none

    if ( .not. fdf_started ) return

    call fdf_refresh( )
    call io_close( fdf_in )
    call io_close( fdf_out )
    fdf_started = .false.
  end subroutine fdf_shutdown

  logical function fdf_block_qmmm( label, unit )
    !! returns "true" and the unit number of the file from which to read
    !! the contents of a block if "label" is associated with a block, and
    !! false if not (unit is set to -1 in this case).
    implicit none
    character(len=*), intent(in)  :: label
    integer         , intent(out) :: unit

    character(len=256) :: token1, filename
    integer            :: iless

    if ( .not. fdf_started ) call qmmm_fdf_init( )

    fdf_block_qmmm = .false.
    unit = -1

    if ( .not. fdf_locate(label) ) return

    token1 = line(first(1):last(1))
    if ( .not. leqi(token1, '%block') ) then
      write(fdf_err,*) 'FDF_BLOCK_QMMM: Not a block:', label
      ! return instead of stopping
      return
    endif

    iless = fdf_search( '<' )
    if ( (iless /= 0) .and. (ntokens > iless) ) then
      ! Read block from file
      filename = line( first(iless+1):last(iless+1) )
      if ( fdf_debug ) write( fdf_log, '(2a)' ) &
        '*Reading block from file', filename
      call fdf_open( filename )
      if ( fdf_search('%dump') /= 0 ) call fdf_dumpfile( label )

      fdf_block_qmmm = .true.
      unit = fdf_in
      return
    endif

    ! Standard block in fdf file. Dump contents
    call fdf_dumpblock( label )
    fdf_block_qmmm = .true.
    unit = fdf_in
  end function fdf_block_qmmm

  subroutine fdf_dumpblock( label )
    !! Dumps block contents starting at the current line.
    implicit none

    character(len=*) :: label

    integer            :: i, lblock
    character(len=128) :: token1

    write( fdf_out, '(/,a79)' ) line
    lblock = 0

    do while ( .true. )
      if ( .not. fdf_getline( ) ) then
        write(fdf_err,'(a,a,a)') &
          'FDF_LOCATE: Block ', label, ' does not end!'
        stop 'FDF'
      endif

      lblock = lblock + 1
      write(fdf_out,'(a79)') line
      token1 = line(first(1):last(1))

      if ( leqi(token1, '%endblock') ) exit
    enddo

    write(fdf_out,*)

    ! Sanity check (optional construct %endblock [ Label [Label] ])
    if ( (ntokens > 1) .and. (fdf_search(label) == 0) ) then
      write(fdf_err,'(a,a,a)') &
           'FDF_LOCATE: Block ', label, ' does not end!'
      stop 'FDF'
    endif

    ! Backspace the lines read.
    do i = 1, lblock
      backspace( fdf_in )
    enddo
  end subroutine fdf_dumpblock

  subroutine fdf_dumpfile( label )
    !! Dumps the contents of a file to fdf_out.
    !! The lines are embedded in a %block ... %endblock pair.
    implicit none

    character(len=*), intent(in) :: label
    character(len=30) :: form
    integer           :: length

    ! Build the right format
    call chrlen_qmmm( label, 0, length )
    write( form, '(a,i2.2,a)' ) '(a,a', length, ',10x,a)'

    write( fdf_out, * )
    write( fdf_out, form ) '%block ', label, &
                           '# Originally in include file.'

    rewind( fdf_in )
    do while (.true.)
      if ( .not. fdf_getline() ) exit
      write( fdf_out, '(a79)' ) line
    enddo

    write( fdf_out, form ) '%endblock ', label, &
                           '# Originally in include file.'
    write( fdf_out, * )
    rewind( fdf_in )
  end subroutine fdf_dumpfile

  subroutine fdf_parse( )
    !! Processes the input line looking for meaningful tokens.

    implicit none
    logical :: intoken, instring
    integer :: c, i, stringdel

    intoken   = .false.
    instring  = .false.
    ntokens   = 0
    stringdel = 0

    do i = 1, len(line)
      c = ichar( line(i:i) )

      if ( iscomment(c) ) then
        ! Is a commented out line.
        if ( instring ) then
          last(ntokens) = i
        else
          exit
        endif

      elseif ( istokch(c) ) then
        ! character allowed in a token...
        if ( .not. intoken ) then
          intoken = .true.
          ntokens = ntokens+1
          first(ntokens) = i
        endif
        last(ntokens) = i

      elseif ( isspecial(c) ) then
        ! character that forms a token by itself...
        if ( .not. instring ) then
          ntokens=ntokens+1
          first(ntokens) = i
          intoken = .false.
        endif
        last(ntokens) = i

      else if ( isdelstr(c) ) then
        ! string delimiter... make sure it is the right one before
        ! closing the string.
        ! if we are currently in a token, the delimiter is appended to it.

        if ( instring ) then
          if ( c == stringdel ) then
            instring = .false.
            intoken = .false.
            stringdel = 0
          else
            last(ntokens) = i
          endif
        else
          if (intoken) then
            last(ntokens) = i
          else
            instring = .true.
            stringdel = c
            intoken = .true.
            ntokens = ntokens+1
            first(ntokens) = i+1
            last(ntokens) = i+1
          endif
        endif
      else
        ! token delimiter...
        if ( instring ) then
          last(ntokens) = i
        else
          if (intoken) intoken=.false.
        endif
      endif
    enddo

    if ( fdf_debug2 ) then
      write(fdf_log,*) '            ',  ntokens, ' tokens:'
      do i = 1, ntokens
        write(fdf_log,*) '                 ', '|', line(first(i):last(i)), '|'
      enddo
    endif

  contains
    logical function isdigit( char )
      integer, intent(in) :: char
      isdigit = (char >= 48) .and. (char <= 57)
    end function isdigit

    logical function isupper( char )
      integer, intent(in) :: char
      isupper = (char >= 65) .and. (char <= 90)
    end function isupper

    logical function islower( char )
      integer, intent(in) :: char
      islower = (char >= 97) .and. (char <= 122)
    end function islower

    logical function isalpha( char )
      integer, intent(in) :: char
      isalpha = isupper(char) .or. islower(char)
    end function isalpha

    logical function isalnum( char )
      integer, intent(in) :: char
      isalnum = isalpha(char) .or. isdigit(char)
    end function isalnum

    logical function isextra( char )
      !! Extra characters allowed in tokens:  $ % * + & - . / @ ^ _ ~
      integer, intent(in) :: char

      isextra = ((char >= 36) .and. (char <= 38)) .or. &
                 (char == 42) .or. (char == 43) .or. (char == 45) .or. &
                 (char == 46) .or. (char == 47) .or. (char == 64) .or. &
                 (char == 94) .or. (char == 95) .or. (char == 126)
    end function isextra

    logical function istokch( char )
      integer, intent(in) :: char
      istokch = isalnum(char) .or. isextra(char)
    end function istokch

    logical function iscomment( char )
      !! Comments are signaled by:  !  #  ;
      integer, intent(in) :: char
      iscomment = (char == 33) .or. (char == 35)  .or. (char == 59)
    end function iscomment

    logical function isdelstr( char )
      !! string delimiters: "  '  `
      integer, intent(in) :: char
      isdelstr = (char == 34) .or. (char == 39)  .or. (char == 96)
    end function isdelstr

    logical function isspecial( char )
      !! Special characters which are tokens by themselves: <
      integer, intent(in) :: char
      isspecial = (char == 60)
    end function isspecial
  end subroutine fdf_parse

  integer function fdf_search( label )
    !! Performs a case-and-punctuation-insensitive search for 'label'
    !! among the tokens in a line.
    implicit none
    character(len=*), intent(in) :: label
    integer :: i

    fdf_search = 0
    do i = 1, ntokens
      if ( labeleq(label,line(first(i):last(i))) ) then
        fdf_search = i
        return
      endif
    enddo

  end function fdf_search

  integer function fdf_integer( label, default )
    !! returns an integer associated with label, or default if label
    !! is not found in the fdf file.
    implicit none
    character(len=*), intent(in) :: label
    integer         , intent(in) :: default

    character(len=10) :: fmtstr

    fdf_integer = default
    if ( .not. fdf_locate(label) ) then
      write( fdf_out, '(a,5x,i10,5x,a)' ) label, default, '# Default value'
      return
    endif

    if ( ntokens == 1 ) then
      write( fdf_err, * ) 'FDF_INTEGER: No value for ', label
      stop
    endif

    write( fmtstr, '(i2.2)' ) last(2) - first(2) +1
    write( fmtstr, '(A)' ) '(i'//trim(fmtstr)//')'
    read( line(first(2):last(2)), fmt = fmtstr ) fdf_integer
    write( fdf_out, '(a,5x,i20)' ) label, fdf_integer
  end function fdf_integer

  logical function labeleq( s1, s2 )
    !! Compares s1 and s2 without regard for case, or appearance
    !! of '_', '.', '-'.
    implicit none

    character(len=*), intent(in) :: s1, s2
    character(len=256) :: n1, n2

    call fdf_pack( s1, n1 )
    call fdf_pack( s2, n2 )
    labeleq = leqi( n1, n2 )
    if ( fdf_debug ) then
      if ( labeleq .and. (.not. leqi(s1,s2)) ) &
        write( fdf_log, '(a,/,a,/,a)' ) '------ Considered equivalent:', s1, s2
    endif
  end function labeleq

  subroutine fdf_pack(s,n)
    !! Removes occurrences of '_ .-'  from s
    implicit none
    character(len=*), intent(in)  :: s
    character(len=*), intent(out) :: n

    character :: c
    integer   :: i, j

    n = ' '
    j = 0
    do i = 1, len(s)
      c = s(i:i)
      if ( .not. issep(ichar(c)) ) then
        j = j+1
        n(j:j) = c
      endif
    enddo

  contains
    logical function issep( char )
      integer, intent(in) :: char
      issep = (char == 95) .or. (char == 46) .or. (char == 45)
    end function issep
  end subroutine fdf_pack

  logical function fdf_getline()
    implicit none
    integer :: ios

    ios = 0
    read( fdf_in, iostat = ios, fmt= '(a)' ) line
    if ( ios /= 0 ) then
      fdf_getline = .false.
      return
    endif

    fdf_getline = .true.
    if ( fdf_debug2 ) write( fdf_log, '(a,a76)' ) '> ', line
    call fdf_parse( )
  end function fdf_getline

  logical function fdf_locate(label)
    !! Searches for label in the fdf hierarchy. if it appears and it
    !! is not part of a comment, the function returns .true. and leaves
    !! the file positioned at the next line. Otherwise, it returns .false.
    !
    ! It supports two kinds of "include" files:
    !   %include filename
    !   Indicates an unconditional opening of filename for
    !   further fdf processing.
    !
    !   Label1 Label2 ... < filename
    !   Indicates that filename should be opened only when
    !   searching for any of the labels indicated.
    !   'filename' should be an fdf file.
    implicit none
    character(len=*), intent(in) :: label
    character(len=256) :: token1, filename
    integer            :: ilabel, iless

    fdf_locate = .false.
    if ( fdf_donothing ) return

    call fdf_refresh( )
    if ( fdf_debug ) write( fdf_log, '(/,a,1x,a)' ) 'Looking for ', label
    rewind( fdf_in )

    do while ( .true. )
      if ( .not. fdf_getline() ) then
        if (ndepth > 1) then
          call fdf_close( )
          cycle
        endif
        if ( fdf_debug ) write( fdf_log, '(a,1x,a)' ) '*Did not find ', label
        return
      endif

      if ( ntokens == 0 ) cycle
      token1 = line(first(1):last(1))

      if ( leqi(token1,'%include') ) then
        ! Include file
        if ( ntokens == 1 ) then
          write( fdf_err, * ) 'FDF: No valid filename after %include'
          stop
        endif
        filename = line(first(2):last(2))
        call fdf_open( filename )
        cycle
      endif

      ilabel = fdf_search(label)

      if ( ilabel /= 0 ) then
        ! Label found...
        if ( leqi(token1,'%block') ) then
          fdf_locate = .true.
          if ( fdf_debug ) write( fdf_log, '(a,1x,a)' ) '*Found ', label
          return
        endif

        iless = fdf_search( '<' )
        if ( (iless /= 0) .and. (ntokens > iless) ) then
          ! Continue search in other file
          filename = line(first(iless+1):last(iless+1))
          call fdf_open( filename )
          cycle
        endif

        ! if we reach this point we must be dealing with a line
        ! of the form 'Label Value'. But we are not interested if
        ! the string appears in the "Value" section.
        if ( ilabel == 1 ) then
          fdf_locate = .true.
          if ( fdf_debug ) write( fdf_log, '(a,1x,a)' ) '*Found ', label
          return
        else
          cycle
        endif
      else
        cycle
      endif
    enddo
  end function fdf_locate

  subroutine fdf_setdebug( level )
    !! Set debugging levels.
    !     level <=0: nothing
    !     level  =1: standard
    !     level >=2: exhaustive
    implicit none
    integer :: level

    if ( level <= 0 ) then
      if ( fdf_debug ) then
        call io_close( fdf_log )
        fdf_debug = .false.
      endif
    else
      if ( .not. fdf_debug ) then
        call io_assign( fdf_log )
        open( fdf_log, file = 'FDF.debug', form = 'formatted', &
              status = 'unknown' )
        rewind( fdf_log )
        fdf_debug = .true.
      endif
    endif

    fdf_debug2 = (level >= 2)
  end subroutine fdf_setdebug

  subroutine fdf_open( filename )
    implicit none
    !! Opens a file for fdf processing.
    character(len=*) :: filename

    integer :: lun
    logical :: file_exists

    ndepth = ndepth + 1
    if ( ndepth > maxdepth ) then
      write( fdf_err, '(a)' ) 'FDF: Too many nested fdf files...'
      stop 'DEPTH'
    endif

    if ( leqi(filename,'stdin') ) then
      lun = 5
      if ( fdf_debug ) write( fdf_log, '(a,i1,a)' )  &
            '--->Reading from Standard Input [depth:', ndepth, '] '
    else
      call io_assign( lun )
      inquire( file = filename, exist = file_exists )

      if ( file_exists ) then
        open( unit = lun, file = filename, status = 'old', &
              form = 'formatted' )
        rewind(lun)
        if ( fdf_debug ) write( fdf_log, '(a,i1,a,a50)' ) &
          '--->Opened [depth:', ndepth, '] ', filename
      else
        write( fdf_err, '(a,a60)' ) 'FDF: Cannot open ',filename
      endif
    endif

    fdf_stack(ndepth) = lun
    fdf_in = lun
  end subroutine fdf_open

  subroutine fdf_close( )
    !! Closes currently opened fdf file, except if it is the original one.
    implicit none

    if ( ndepth > 1 ) then
      call io_close( fdf_in )
      if ( fdf_debug ) write( fdf_log, '(a,i1,a)' ) '--->Closed [depth:', &
                                                    ndepth, ']'
      ndepth = ndepth -1
      fdf_in = fdf_stack(ndepth)
    endif
  end subroutine fdf_close

  subroutine fdf_refresh( )
    !! Closes all the open files in the stack (except the first).
    !! Failure to do so would imply that the next Label is searched first in
    !! the 'deeper' files. fdf_locate calls fdf_refresh before doing anything.
    implicit none
    integer :: i

    do i = ndepth, 1 , -1
      call fdf_close( )
    enddo
  end subroutine fdf_refresh

  subroutine chrlen_qmmm( string, nchar, lchar )
    !! chrlen_qmmm accepts a string of nchar characters and returns lchar,
    !! the length of the string up to the last nonblank, nonnull.
    implicit none
    character(len=*), intent(in)  :: string
    integer         , intent(in)  :: nchar
    integer         , intent(out) :: lchar

    integer :: ncopy, i

    ncopy = nchar
    if ( ncopy <= 0 ) ncopy = len( string )

    do i = 1, ncopy
      lchar = ncopy +1 - i
      if ( (string(lchar:lchar) /= ' ') .and. &
           (string(lchar:lchar) /= char(0)) ) return
    enddo
    lchar=0
  end subroutine chrlen_qmmm

  subroutine chrcap( string, nchar )
    !! chrcap accepts a string of nchar characters and replaces
    !!  any lowercase letters by uppercase ones.
    implicit none
    integer         , intent(in)  :: nchar
    character(len=*), intent(out) :: string

    integer   :: ncopy, i, itemp

    ncopy = nchar
    if ( ncopy <= 0 ) ncopy = len( string )
    do i = 1, ncopy
      if ( lge(string(i:i),'a') .and. lle(string(i:i),'z')) then
        itemp = ichar(string(i:i)) + ichar('A') - ichar('a')
        string(i:i) = char(itemp)
      endif
    enddo
  end subroutine chrcap

  logical function leqi( strng1, strng2 )
    !! Case-insensitive lexical equal-to comparison
    implicit none

    character(len=*), intent(in) :: strng1
    character(len=*), intent(in) :: strng2

    character :: s1, s2
    integer   :: len1, len2, lenC, i
    len1 = len( strng1 )
    len2 = len( strng2 )
    lenC = min( len1, len2 )

    leqi = .false.
    do i = 1, lenC
      s1 = strng1(i:i)
      s2 = strng2(i:i)
      call chrcap(S1,1)
      call chrcap(S2,1)
      if ( S1 /= S2 ) return
    enddo

    if ( (len1 > lenC) .and. (strng1(lenC+1:len1) /= ' ') ) return
    if ( (len2 > lenC) .and. (strng2(lenC+1:len2) /= ' ') ) return
    leqi = .true.
  end function leqi

end module m_qmmm_fdf
