A QM/MM driver based on Siesta, developed by Carlos Sanz, based on previous work
by Crespo et al.

See the Docs directory for features and documentation, and INSTALL.md for important notes about
installation.

Work is in progress to modernize this code, and to make it work with modern versions of Siesta.

