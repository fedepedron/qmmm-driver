module restr_subs
  !! This module contains r0utines that deal with harmonic restraints.
  !! There are eight types of restraints available:
  !!   1) r1 - r2 coupled  2) Distance         3) Angle
  !!   4) Dihedral         5) r1 + r2 coupled  6) (r1+r2) - (r3+r4) coupled
  !!   7) average distance from atom to 2-4 atoms
  !!   8) c1*r1 + c2*r2 + c3*r3 + ....
  use precision, only : dp
  implicit none
  public :: restr_read
  public :: restr_calc
  public :: restr_update

  private
  integer  :: nrestr
    !! Number of constraints.
  integer  :: typerestr(20)
    !! Type of each of the restraints.
  real(dp) :: kforce(20)
    !! Force constant of each of the restraints.
  real(dp) :: r_ini
    !! Initial value of the distance restraint.
  real(dp) :: r_fin
    !! Final value of the distance restraint.
  integer  :: atmsrestr(20,20)
    !! Indices of the restrained atoms.
  real(dp) :: dr
    !! Increments for the distance restraint.
  real(dp) :: r0(20)
    !! Equilibrium value for each restraint.
  integer  :: ndists(20)
    !! Number of coupled distances in each restraint.
  real(dp) :: coef(20,10)
    !! Weights for each term of the restraint.

contains

  subroutine restr_read( nsteprestr, restrlog, nrestr_out )
    !! This subroutine reads the system harmonic restraints.
    use fdf       , only : fdf_string
    use m_qmmm_fdf, only : fdf_block_qmmm
    use precision , only : dp
    use sys       , only : die

    implicit none
    integer, intent(out)   :: nsteprestr
      !! Change the restraint equilibrium distances from r_ini to r_fin
      !! in this number of steps.
    logical, intent(inout) :: restrlog
      !! Whether we do a restrained optimization. If we find no restraints,
      !! This is set to false.
    integer, intent(out)   :: nrestr_out
      !! The total number of restraints present.

    integer            :: irs, junit, iunit, irestr, ios, iline
    logical            :: found
    real(dp)           :: dummyr
    character(len=1)   :: exp
    character(len=224) :: slabel
    character(len=255) :: fname

    ! Externals
    external           :: io_assign, io_close

    found = .false.

    ! Read input variables
    if ( .not. fdf_block_qmmm('ConstrainedOpt',iunit) ) &
      call die('restr_read: You must specify the ConstraindeOpt block')

    ios = 0
    read( iunit, fmt = '(A1)', advance = 'no', iostat = ios ) exp
    if ( ios /= 0 ) &
      call die( 'restr_read: Problem while reading ConstrainedOpt block.' )
    if ( exp == '%' ) then
      write( 6, * ) "WARNING: ConstrainedOpt block found, but no restraints."
      restrlog   = .false.
      nsteprestr = 0
      return
    endif

    read( iunit, *, iostat = ios ) exp, nrestr
    if ( ios /= 0 ) &
      call die( 'restr_read: Problem while reading ConstrainedOpt block.' )
    if ( nrestr > 20 ) &
      call die('restr_read: nrestr must be lower than 20')
    nrestr_out = nrestr

    read( iunit, *, iostat = ios ) exp, nsteprestr
    if ( ios /= 0 ) &
      call die( 'restr_read: Problem while reading ConstrainedOpt block.' )

    if ( nsteprestr == 0 ) then
      call die('restr_read: nsteprestr must be larger than 0')
    elseif( nsteprestr > 100 ) then
      call die('restr_read: nsteprestr must be lower than 100')
    endif

    do irestr = 1, nrestr
      read( iunit, *, iostat = ios ) exp, typerestr(irestr)
      if ( ios /= 0 ) &
        call die( 'restr_read: Problem while reading ConstrainedOpt block.' )
      read( iunit, *, iostat = ios ) exp, kforce(irestr)
      if ( ios /= 0 ) &
        call die( 'restr_read: Problem while reading ConstrainedOpt block.' )

      if ( irestr == 1 ) then
        read( iunit, *, iostat = ios ) exp, r_ini, exp, r_fin
      else
        read( iunit, *, iostat = ios ) exp, r0(irestr)
      endif
      if ( ios /= 0 ) &
        call die( 'restr_read: Problem while reading ConstrainedOpt block.' )

      irs = 0
      select case( typerestr(irestr) )
      case (1,4,5)
        read( iunit, *, iostat = ios ) exp,       &
          atmsrestr(irestr,1), atmsrestr(irestr,2), &
          atmsrestr(irestr,3), atmsrestr(irestr,4)

      case (2)
        read( iunit, *, iostat = ios ) exp, &
          atmsrestr(irestr,1), atmsrestr(irestr,2)

      case (3)
        read( iunit, *, iostat = ios ) exp,       &
          atmsrestr(irestr,1), atmsrestr(irestr,2), &
          atmsrestr(irestr,3)

      case (6)
        read( iunit, *, iostat = ios ) exp,       &
          atmsrestr(irestr,1), atmsrestr(irestr,2), &
          atmsrestr(irestr,3), atmsrestr(irestr,4), &
          atmsrestr(irestr,5), atmsrestr(irestr,6), &
          atmsrestr(irestr,7), atmsrestr(irestr,8)

      case (7)
        read( iunit, *, iostat = ios ) exp,       &
          atmsrestr(irestr,1), atmsrestr(irestr,2), &
          atmsrestr(irestr,3), atmsrestr(irestr,4), &
          atmsrestr(irestr,5)

      case (8)
        read( iunit, *, iostat = ios ) exp, ndists(irestr)

        read( iunit, fmt = '(A1)', advance = 'no', iostat = ios ) exp
        do irs = 1, ndists(irestr)-1
          read( iunit, fmt = '(F5.2)', advance = 'no', iostat = ios ) &
            coef(irestr,irs)
        enddo
        read( iunit, *, iostat = ios ) coef(irestr,ndists(irestr))

        read( iunit, fmt = '(A1)', advance = 'no', iostat = ios ) exp
        do irs = 1, 2*ndists(irestr)-1
          read( iunit, fmt = '(I6)', advance = 'no', iostat = ios ) &
            atmsrestr(irestr,irs)
        enddo
        read( iunit, *, iostat = ios ) &
          atmsrestr(irestr,ndists(irestr)*2)

      case default
        call die( 'restr_read: Wrong restraint type.' )
      end select
      if ( ios /= 0 ) &
        call die( 'restr_read: Problem while reading ConstrainedOpt block.' )

      if ( irs > 20 ) &
        call die('restr_read: Number of atoms within must be lower than 20.')
    enddo

    write(6,'(/,a)') 'restr_read: Doing a restrained optimization run.'

    ! Calculates the initial equilibrium r0 for all types of constraints.
    dr    = ( r_fin - r_ini ) / nsteprestr
    r0(1) = r_ini

    if ( nsteprestr == 1 ) nsteprestr = 0
    if ( abs(r_fin - r_ini) < 1.0e-14_dp ) nsteprestr = 0

    ! Reads from .rce of a former run.
    slabel = fdf_string( 'SystemLabel', 'siesta' )
    fname  = trim(slabel) // '.rce'

    inquire( file = fname, exist = found )
    if ( found ) then
      call io_assign( junit )
      open( junit, file = fname )

      iline = 0
      ios   = 0
      do while ( ios == 0 )
        iline = iline +1
        read( junit, *, iostat = ios ) dummyr
        if ( ios < 0 ) exit
        if ( ios > 0 ) &
          call die('restr_read: Problem while reading form rce file.')
      enddo
      call io_close( junit )

      iline = iline -1

      if ( nsteprestr > iline ) then
        nsteprestr = nsteprestr - iline
        r0(1)      = r0(1) + dr * iline

        write(6,'(/,a)') &
          'restr_read: Re-starting a Constrained optimization run.'
      endif
    endif !found
  end subroutine restr_read

  subroutine restr_calc( rt, nsteprestr, natot, r_inp, fdummy, istep, &
                         istepconstr, ucell, lattice_type )
    !! Calculates restrained energies and forces contributions.
    use functions, only : dist_v2, scalar_v2, angle_v2, dihedro2_v2, &
                          diheforce2_v2
    use precision, only : dp
    use qmmm_pbc , only : pbc_displ_vector, reccel
    use sys      , only : die

    implicit none
    integer  , intent(in)    :: nsteprestr
      !! Change the restraint equilibrium distances from r_ini to r_fin
      !! in this number of steps.
    integer  , intent(in)    :: natot
      !! Total number of atoms.
    integer  , intent(in)    :: istep
      !! Current MD step.
    integer  , intent(in)    :: istepconstr
      !! Current constrained optimization step.
    real(dp) , intent(in)    :: r_inp(3,natot)
      !! Atomic cartesian coordinates.
    real(dp) , intent(in)    :: ucell(3,3)
      !! Unit cell vectors (in a.u.)
    character, intent(in)    :: lattice_type
      !! Type of periodic lattice used.
    real(dp) , intent(out)   :: rt(20)
      !! Total R of a given coordinate restraint.
    real(dp) , intent(inout) :: fdummy(3,natot)
      !! Dummy forces array over which we add restrained forces.


    integer  :: npi, irestr, iat, ratom(8), idst
    real(dp) :: fce, fnew(3,10), rp(3), r12, r34, r32, r56, r78, rdst(3),    &
                rdst12(3), rdst32(3), rdst34(3), rdst56(3), rdst78(3),       &
                ang_conv, fdihe(12), Fterm, rtot, req, kf, scal, dscalar(3), &
                dr12r32(3), amber_cell(3,3), amber_kcell(3,3)

    real(dp), allocatable :: rclas(:,:)

    ang_conv = 2.0_dp * acos(-1.0_dp ) / 180.0_dp

    ! Convert units from atomic to angstrom.
    allocate( rclas(3,natot) )
    rclas(1:3,1:natot)  = r_inp(1:3,1:natot) * 0.529177_dp
    amber_cell(1:3,1:3) = ucell(1:3,1:3)     * 0.529177_dp
    call reccel( 3, amber_cell, amber_kcell, 0 )

    ! Loop over all restraints.
    do irestr = 1, nrestr
      fnew = 0.0_dp

      select case( typerestr(irestr) )
      case (1) ! Distance subtraction.
        ratom(1:4) = atmsrestr(irestr,1:4)
        rdst(1:3)  = rclas(1:3,ratom(1)) - rclas(1:3,ratom(2))

        call pbc_displ_vector( lattice_type, amber_cell, amber_kcell, &
                               rdst(1), rdst(2), rdst(3) )
        r12 = dist_v2( rdst(1), rdst(2), rdst(3) )

        rdst(1:3) = rclas(1:3,ratom(3)) - rclas(1:3,ratom(4))
        call pbc_displ_vector( lattice_type, amber_cell, amber_kcell, &
                               rdst(1), rdst(2), rdst(3) )
        r34  = dist_v2( rdst(1), rdst(2), rdst(3) )
        rtot = r34 - r12

        rt(irestr) = rtot
        req        = r0(irestr)
        kf         = kforce(irestr)

        ! Forces over atoms 1 and 2: dr12
        rdst(1:3) = rclas(1:3,ratom(1)) - rclas(1:3,ratom(2))
        call pbc_displ_vector( lattice_type, amber_cell, amber_kcell, &
                               rdst(1), rdst(2), rdst(3) )

        rdst(1:3) =   rdst(1:3) * 2.0_dp * kf * (rtot - req) / r12
        fnew(:,1) =   rdst(1:3)
        fnew(:,2) = - rdst(1:3)

        ! Forces over atoms 3 and 4: dr34
        rdst(1:3) = rclas(1:3,ratom(3)) - rclas(1:3,ratom(4))
        call pbc_displ_vector( lattice_type, amber_cell, amber_kcell, &
                               rdst(1), rdst(2), rdst(3) )

        rdst(1:3) =   rdst(1:3) * 2.0_dp * kf * (rtot - req) / r34
        fnew(:,3) = - rdst(1:3)
        fnew(:,4) =   rdst(1:3)

        ! Adding fnew to fdummy.
        do iat = 1, 4
          fdummy(1:3,ratom(iat))= fdummy(1:3,ratom(iat)) + fnew(1:3,iat)
        enddo

      case (2) ! Simple distance restraint.
        ratom(1:2) = atmsrestr(irestr,1:2)
        rdst(1:3)  = rclas(1:3,ratom(1)) - rclas(1:3,ratom(2))

        call pbc_displ_vector( lattice_type, amber_cell, amber_kcell, &
                               rdst(1), rdst(2), rdst(3) )
        rtot       = dist_v2( rdst(1), rdst(2), rdst(3) )
        rt(irestr) = rtot
        req        = r0(irestr)
        kf         = kforce(irestr)

        rdst(1:3) =  rdst(1:3) * 2.0_dp * kf * (rtot - req) / rtot
        fnew(:,1) = -rdst(1:3)
        fnew(:,2) =  rdst(1:3)

        ! Adding fnew to fdummy.
        fdummy(1:3,ratom(1)) = fdummy(1:3,ratom(1)) + fnew(1:3,1)
        fdummy(1:3,ratom(2)) = fdummy(1:3,ratom(2)) + fnew(1:3,2)

      case (3) ! Angle restraint.
        ratom(1:3)  = atmsrestr(irestr,1:3)
        rdst12(1:3) = rclas(1:3,ratom(1)) - rclas(1:3,ratom(2))

        call pbc_displ_vector( lattice_type, amber_cell, amber_kcell, &
                               rdst12(1), rdst12(2), rdst12(3) )

        rdst32(1:3) = rclas(1:3,ratom(3)) - rclas(1:3,ratom(2))
        call pbc_displ_vector( lattice_type, amber_cell, amber_kcell, &
                               rdst32(1), rdst32(2), rdst32(3) )
        rtot = angle_v2( rdst12(1), rdst12(2), rdst12(3), &
                         rdst32(1), rdst32(2), rdst32(3) )
        rt(irestr) = rtot
        req        = r0(irestr)
        kf         = kforce(irestr)

        scal = scalar_v2( rdst12(1), rdst12(2), rdst12(3), &
                          rdst32(1), rdst32(2), rdst32(3) )
        r12 = dist_v2( rdst12(1), rdst12(2), rdst12(3) )
        r32 = dist_v2( rdst32(1), rdst32(2), rdst32(3) )
        fce = kf * (rtot - req) * ang_conv

        scal = scal / ( r12 * r12 * r32 * r32 )

        ! Forces over atom 1:
        dr12r32(1:3) = r32 * rdst32(1:3) / r12
        rdst(1:3) = rdst(1:3) * ( 1.0_dp / ( r12 * r32 ) - scal * dr12r32(1:3) )
        rdst(1:3) = rdst(1:3) * fce / sqrt( 1.0_dp - scal )

        fnew(1:3,1) = rdst(1:3)

        ! Forces over atom 2:
        dscalar(1:3) = - ( rdst12(1:3) + rdst32(1:3) )
        dr12r32(1:3) = - ( r32 * rdst12(1:3) / r12 + r12 * rdst32(1:3) / r32 )

        rdst(1:3) = dscalar(1:3) / ( r12 * r32 ) - scal * dr12r32(1:3)
        rdst(1:3) = rdst(1:3) * fce / sqrt( 1.0_dp - scal )

        fnew(1:3,2) = rdst(1:3)

        ! Forces over atom 3:
        fnew(1:3,3) = -fnew(1:3,1)

        ! Adding fnew to fdummy.
        do iat = 1, 3
          fdummy(1:3,ratom(iat))= fdummy(1:3,ratom(iat)) + fnew(1:3,iat)
        enddo

      case (4) ! Dihedral restraint.
        ratom(1:4) = atmsrestr(irestr,1:4)
        rtot = dihedro2_v2( &
          rclas(1,ratom(1)), rclas(2,ratom(1)), rclas(3,ratom(1)), &
          rclas(1,ratom(2)), rclas(2,ratom(2)), rclas(3,ratom(2)), &
          rclas(1,ratom(3)), rclas(2,ratom(3)), rclas(3,ratom(3)), &
          rclas(1,ratom(4)), rclas(2,ratom(4)), rclas(3,ratom(4)), &
          amber_cell, amber_kcell, lattice_type )

        req = r0(irestr)
        kf  = kforce(irestr)
        if ( (req < 90.0_dp ) .and. (rtot > 180.0_dp) ) rtot = rtot - 360.0_dp
        if ( (req > 270.0_dp) .and. (rtot < 180.0_dp) ) rtot = rtot + 360.0_dp

        Fterm = kf * (rtot - req) * ang_conv

        call diheforce2_v2( natot, rclas, ratom(1), ratom(2), ratom(3), &
                            ratom(4), 1, Fterm, fdihe, amber_cell,      &
                            amber_kcell, lattice_type)
        fnew(1:3,1) = fdihe(1:3)

        call diheforce2_v2( natot, rclas, ratom(1), ratom(2), ratom(3), &
                            ratom(4), 2, Fterm, fdihe, amber_cell,      &
                            amber_kcell, lattice_type)
        fnew(1:3,2) = fdihe(4:6)

        call diheforce2_v2( natot, rclas, ratom(1), ratom(2), ratom(3), &
                            ratom(4), 3, Fterm, fdihe, amber_cell,      &
                            amber_kcell, lattice_type)
        fnew(1:3,3) = fdihe(7:9)

        call diheforce2_v2( natot, rclas, ratom(1), ratom(2), ratom(3), &
                            ratom(4), 4, Fterm, fdihe, amber_cell,      &
                            amber_kcell, lattice_type)
        fnew(1:3,4) = fdihe(10:12)

        ! Adding fnew to fdummy.
        if ( (.not. ((rtot < 0.0_dp) .or. (rtot > 180.0_dp))) .or. &
             (rtot > 360_dp) ) then
          fnew(1:3,1:4) = (-1.0_dp) * fnew(1:3,1:4)
        elseif ( ((rtot > 180.0_dp) .and. (rtot < 360.0_dp)) .or. &
                 (rtot < 0.0_dp) ) then
          fnew(1:3,1:4) = fnew(1:3,1:4)
        else
          call die('restr_calc: Wrong dihedral angle value.')
        endif
        do iat = 1, 4
          fdummy(1:3,ratom(iat))= fdummy(1:3,ratom(iat)) + fnew(1:3,iat)
        enddo
        rt(irestr) = rtot

      case (5) ! Distance addition.
        ratom(1:4)  = atmsrestr(irestr,1:4)
        rdst12(1:3) = rclas(1:3,ratom(1)) - rclas(1:3,ratom(2))
        rdst34(1:3) = rclas(1:3,ratom(3)) - rclas(1:3,ratom(4))

        call pbc_displ_vector( lattice_type, amber_cell, amber_kcell, &
                               rdst12(1), rdst12(2), rdst12(3) )
        call pbc_displ_vector( lattice_type, amber_cell, amber_kcell, &
                               rdst34(1), rdst34(2), rdst34(3) )
        r12 = dist_v2( rdst12(1), rdst12(2), rdst12(3) )
        r34 = dist_v2( rdst34(1), rdst34(2), rdst34(3) )

        rtot = r34 + r12

        rt(irestr) = rtot
        req        = r0(irestr)
        kf         = kforce(irestr)

        ! Atoms 1 and 2: dr12.
        rdst(1:3)   =  rdst12(1:3) * 2.0_dp * kf * (rtot - req) / r12
        fnew(1:3,1) = -rdst(1:3)
        fnew(1:3,2) =  rdst(1:3)

        ! Atoms 3 and 4: dr34.
        rdst(1:3)   =  rdst34(1:3) * 2.0_dp * kf * (rtot - req) / r34
        fnew(1:3,3) = -rdst(1:3)
        fnew(1:3,4) =  rdst(1:3)

        ! Adding fnew to fdummy
        do iat = 1, 4
          fdummy(1:3,ratom(iat))= fdummy(1:3,ratom(iat)) + fnew(1:3,iat)
        enddo

      case (6) ! Subtraction of two distances additions.
        ratom(1:8)  = atmsrestr(irestr,1:8)
        rdst12(1:3) = rclas(1:3,ratom(1)) - rclas(1:3,ratom(2))
        rdst34(1:3) = rclas(1:3,ratom(3)) - rclas(1:3,ratom(4))
        rdst56(1:3) = rclas(1:3,ratom(5)) - rclas(1:3,ratom(6))
        rdst78(1:3) = rclas(1:3,ratom(7)) - rclas(1:3,ratom(8))

        call pbc_displ_vector( lattice_type, amber_cell, amber_kcell, &
                               rdst12(1), rdst12(2), rdst12(3) )
        call pbc_displ_vector( lattice_type, amber_cell, amber_kcell, &
                               rdst34(1), rdst34(2), rdst34(3) )
        call pbc_displ_vector( lattice_type, amber_cell, amber_kcell, &
                               rdst56(1), rdst56(2), rdst56(3) )
        call pbc_displ_vector( lattice_type, amber_cell, amber_kcell, &
                               rdst78(1), rdst78(2), rdst78(3) )

        r12  = dist_v2( rdst12(1), rdst12(2), rdst12(3) )
        r34  = dist_v2( rdst34(1), rdst34(2), rdst34(3) )
        r56  = dist_v2( rdst56(1), rdst56(2), rdst56(3) )
        r78  = dist_v2( rdst78(1), rdst78(2), rdst78(3) )
        rtot = ( r34 + r12 ) - ( r56 + r78 )

        rt(irestr) = rtot
        req        = r0(irestr)
        kf         = kforce(irestr)

        ! Atoms 1 and 2. dr12.
        rdst(1:3)   =  rdst12(1:3) * 2.0_dp * kf * (rtot - req) / r12
        fnew(1:3,1) = -rdst(1:3)
        fnew(1:3,2) =  rdst(1:3)

        ! Atoms 3 and 4. dr34.
        rdst(1:3)   =  rdst34(1:3) * 2.0_dp * kf * (rtot - req) / r34
        fnew(1:3,3) = -rdst(1:3)
        fnew(1:3,4) =  rdst(1:3)

        ! Atoms 5 and 6. dr56.
        rdst(1:3)   =  rdst56(1:3) * 2.0_dp * kf * (rtot - req) / r56
        fnew(1:3,5) =  rdst(1:3)
        fnew(1:3,6) = -rdst(1:3)

        ! Atoms 7 and 8. dr56.
        rdst(1:3)   =  rdst78(1:3) * 2.0_dp * kf * (rtot - req) / r78
        fnew(1:3,7) =  rdst(1:3)
        fnew(1:3,8) = -rdst(1:3)

        do iat = 1, 8
          fdummy(1:3,ratom(iat))= fdummy(1:3,ratom(iat)) + fnew(1:3,iat)
        enddo

      case (7) ! Average distance to 2-4 atoms.
        npi = 0
        do iat = 2, 5
          if ( atmsrestr(irestr,iat) /= 0 ) npi = npi +1
        enddo
        if ( npi == 0 ) &
          call die( 'restr_calc: atoms in average can not be zero.' )

        ratom(1:5)=atmsrestr(irestr,1:5)
        rp(1:3) = ( rclas(1:3,ratom(2)) + rclas(1:3,ratom(3)) + &
                    rclas(1:3,ratom(4)) + rclas(1:3,ratom(5)) ) / npi

        rdst12(1:3) = rclas(1:3,ratom(1)) - rp(1:3)
        call pbc_displ_vector( lattice_type, amber_cell, amber_kcell, &
                               rdst12(1), rdst12(2), rdst12(3) )
        rtot = dist_v2( rdst12(1), rdst12(2), rdst12(3) )

        rt(irestr) = rtot
        req        = r0(irestr)
        kf         = kforce(irestr)

        ! Force is calculated only over atom n°1.
        ! Is this physically correct though???
        fnew(1:3,1) = -rdst12(1:3) * 2.0_dp * kf * (rtot - req) / rtot

        fdummy(1:3,ratom(1)) = fdummy(1:3,ratom(1)) + fnew(1:3,1)

      case (8) ! Coefficients * positions.

        rtot = 0.0_dp
        do idst = 1, ndists(irestr)
          ratom(1) = atmsrestr(irestr, idst)
          ratom(2) = atmsrestr(irestr, idst+1)

          rdst12(1:3) = rclas(1:3,ratom(1)) - rclas(1,ratom(2))
          call pbc_displ_vector( lattice_type, amber_cell, amber_kcell, &
                                 rdst12(1), rdst12(2), rdst12(3) )
          r12 = dist_v2( rdst12(1), rdst12(2), rdst12(3) )

          rtot = rtot + coef(irestr,idst) * r12
        enddo

        rt(irestr) = rtot
        req        = r0(irestr)
        kf         = kforce(irestr)

        ! We calculate forces by pairs.
        do idst = 1, ndists(irestr)
          ratom(1) = atmsrestr(irestr, idst)
          ratom(2) = atmsrestr(irestr, idst+1)

          rdst12(1:3) = rclas(1:3,ratom(1)) - rclas(1,ratom(2))
          call pbc_displ_vector( lattice_type, amber_cell, amber_kcell, &
                                 rdst12(1), rdst12(2), rdst12(3) )
          r12 = dist_v2( rdst12(1), rdst12(2), rdst12(3) )

          rdst(1:3)   =  rdst12(1:3) * 2.0_dp * kf * (rtot - req) / r12
          fnew(1:3,1) = -rdst(1:3) * coef(irestr,idst)
          fnew(1:3,2) =  rdst(1:3) * coef(irestr,idst)

          ! Adding fnew to fdummy
          fdummy(1:3,ratom(1))= fdummy(1:3,ratom(1)) + fnew(1:3,1)
          fdummy(1:3,ratom(2))= fdummy(1:3,ratom(2)) + fnew(1:3,2)
        enddo
      case default

      end select ! Restraint type.

      ! If we are in the first step, write outputs.
      if ( istep == 1 ) then

        if ( irestr == 1 ) then
          write(*,'(/,A)')     'restr_calc: variable restraint'
        else
          write(*,'(/,A)')     'restr_calc: constant restraints'
        endif
        write(*,'(A,i4)')    'irestr   :', irestr

        if ( istepconstr == 1 ) then
          write(*,'(A,i4)')    'type     :', typerestr(irestr)

          select case( typerestr(irestr) )
          case (1,4,5)
            write(*,'(A,4i4)')   'atoms    :', &
              atmsrestr(irestr,1), atmsrestr(irestr,2), &
              atmsrestr(irestr,3), atmsrestr(irestr,4)

          case (2)
            write(*,'(A,2i4)')   'atoms    :', &
              atmsrestr(irestr,1), atmsrestr(irestr,2)

          case (3)
            write(*,'(A,3i4)')   'atoms    :', &
              atmsrestr(irestr,1), atmsrestr(irestr,2), &
              atmsrestr(irestr,3)

          case (6)
            write(*,'(A,8i4)')   'atoms    :', &
              atmsrestr(irestr,1), atmsrestr(irestr,2), &
              atmsrestr(irestr,3), atmsrestr(irestr,4), &
              atmsrestr(irestr,5), atmsrestr(irestr,6), &
              atmsrestr(irestr,7), atmsrestr(irestr,8)

          case (7)
            write(*,'(A,5i4)')   'atoms    :', &
              atmsrestr(irestr,1), atmsrestr(irestr,2), &
              atmsrestr(irestr,3), atmsrestr(irestr,4), &
              atmsrestr(irestr,5)

          case (8)
            write(*,'(A,i4)')    'ndists   :', ndists(irestr)
            do idst = 1, ndists(irestr)
              write(*,'(A,F5.2)')  'coefs    :', coef(irestr,idst)
            enddo
            do idst = 1, 2*ndists(irestr)
              write(*,'(A,i4)')    'atoms    :', atmsrestr(irestr,idst)
            enddo

          case default

          end select

          write(*,'(A,F8.3)')  'constant :', kforce(irestr)

          if ( irestr == 1 ) then
            write(*,'(A,i4)')    'steps    :', nsteprestr
            write(*,'(A,F8.3)')  'initial  :', r_ini
            write(*,'(A,F8.3)')  'final    :', r_fin
          endif
        endif

        write(*,'(A,F8.3)')  'targeted :', r0(irestr)
        write(*,'(A,F8.3)')  'current  :', rt(irestr)
      endif
    enddo ! Loop ver restraints.

    deallocate( rclas )
  end subroutine restr_calc

  subroutine restr_update( rt, Etot )
    !! Updates r0 if necessary and prints energy.
    use precision, only: dp

    implicit none
    real(dp), intent(in) :: rt
      !! Total reaction coordinate.
    real(dp), intent(in) :: Etot
      !! Total energy.
    r0 = r0 + dr

    write(6,*)
    write(6,'(a)') 'restr_calc: Optimized Reaction Coordinate'
    write(6,'(a,F10.5)') 'Reaction Coordinate (Ang) : ', rt
    write(6,'(a,F12.5)') 'Total Energy (eV) : ', Etot * 13.60580_dp
  end subroutine restr_update

end module restr_subs