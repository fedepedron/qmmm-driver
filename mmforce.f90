module mmforces_m
   !! This module contains the subroutines needed to calculate
   !! electron and nuclei forces over point charges (i.e. atoms).

   implicit none
   public :: mmforce
   public :: mmforce_ewald

contains

  subroutine mmforce( na_qm, na_mm, natot, ntpl, ntm, r, f,stress, pc, &
                      ucell, rcut_rho, dvol, Rho, lattice_type )

    !! Computes forces exerted by electrons and nuclei over point charges using
    !! a cut-off scheme.
    use precision     , only : dp, grid_p
    use qmmm_neighbour, only : num_mmvecs, grid_veclist, grid_nr
    use qmmm_pbc      , only : reccel, pbc_displ_vector

    implicit none
    integer     , intent(in) :: na_qm
      !! Number of QM atoms.
    integer     , intent(in) :: na_mm
      !! Number of MM atoms.
    integer     , intent(in) :: natot
      !! Total (QM+MM) number of atoms.
    integer     , intent(in) :: ntpl
      !! Total number of grid points.
    integer     , intent(in) :: ntm(3)
      !! Number of mesh points along each axis.
    real(dp)    , intent(in) :: r(3,natot)
      !! Atomic positions.
    real(dp)    , intent(in) :: pc(na_mm)
      !! Atomic (classical) partial charges.
    real(dp)    , intent(in) :: ucell(3,3)
      !! Unit cell vectors.
    real(dp)    , intent(in) :: rcut_rho
      !! Cut-off radius grid point-charge interactions.
    real(dp)    , intent(in) :: dvol
      !! Volume of a point of the unit cell.
    real(grid_p), intent(in) :: Rho(ntpl)
      !! Electron density in the grid.
    character   , intent(in) :: lattice_type
      !! Type of cell lattice.
    real(dp)    , intent(inout) :: f(3,natot)
      !! Atomic forces.
    real(dp)    , intent(inout) :: stress(3,3)
      !! Cell stress.

    integer  :: ix, iy, iz, imesh, iat, icrd, ivec, js
    real(dp) :: drij(3), rcut_rho2, stress_fact, dist, xm, ym, zm, dE, &
                kcell(3,3)

    if ( num_mmvecs == 0 ) return
    rcut_rho2   = ( rcut_rho / 0.529177_dp ) ** 2
    stress_fact = 1.0_dp / ( dVol * real( ntpl, kind=dp ) )

    call reccel( 3, ucell, kcell, 0 )

    ! For now, we put a range of orbital equals to rmax0 (bohrs).
    ! Loop over the mesh points
    do iz = 0, ntm(3) -1
    do iy = 0, ntm(2) -1
    do ix = 0, ntm(1) -1
      imesh = 1 + ix + ntm(1) * iy + ntm(1) * ntm(2) * iz

      ! We use only grid points where there is density of charge.
      if ( abs( Rho(imesh) ) > 0.0_dp ) then
        xm = ucell(1,1) * ix / ntm(1) + ucell(1,2) * iy / ntm(2) &
           + ucell(1,3) * iz / ntm(3)
        ym = ucell(2,1) * ix / ntm(1) + ucell(2,2) * iy / ntm(2) &
           + ucell(2,3) * iz / ntm(3)
        zm = ucell(3,1) * ix / ntm(1) + ucell(3,2) * iy / ntm(2) &
           + ucell(3,3) * iz / ntm(3)

        do iat = 1, num_mmvecs ! Loop over MM atoms
          js = grid_veclist(iat)

          if ( lattice_type == 'D' ) then
            drij(1) = xm - r(1,js) + grid_nr(1,iat) * ucell(1,1)
            drij(2) = ym - r(2,js) + grid_nr(2,iat) * ucell(2,2)
            drij(3) = zm - r(3,js) + grid_nr(3,iat) * ucell(3,3)
          else
            drij(1) = xm - r(1,js)
            drij(2) = ym - r(2,js)
            drij(3) = zm - r(3,js)

            do icrd = 1, 3
            do ivec = 1, 3
               drij(icrd) = drij(icrd) + grid_nr(ivec,iat) * ucell(icrd,ivec)
            enddo
            enddo
          endif
          call pbc_displ_vector( lattice_type, ucell, kcell, &
                                 drij(1), drij(2), drij(3) )
          dist = drij(1) * drij(1) + drij(2) * drij(2) + drij(3) * drij(3)


          ! Forces exerted on point charges by electrons.
          De = 0.0_dp
          if ( dist > rcut_rho2 ) then

            ! If our density point and point charge are too close,
            ! this might diverge. Thus, we avoid it.
            dist = ( 1.0_dp / sqrt( dist ) ) ** 3
            De   = -2.0_dp * dvol * Rho(imesh) * pc(js-na_qm) * dist
          endif

          do ivec = 1, 3
            f(ivec,js) = f(ivec,js) - De * drij(ivec)

            do icrd = 1, 3
              stress(icrd,ivec) = stress(icrd,ivec) &
                               - stress_fact * r(icrd,js) * De * drij(ivec)
            enddo
          enddo
        enddo ! MM atoms
      endif   ! abs(Rho(imesh)) > 0
    enddo     ! grid X
    enddo     ! grid Y
    enddo     ! grid Z
  end subroutine mmforce

  subroutine mmforce_ewald( na_qm, na_mm, natot, ntpl, ntm ,r, f, stress, pc,  &
                            ucell, rcut_rho, ewald_alpha, kewald_cutoff, dvol, &
                            Rho, lattice_type )
    !! Computes forces exerted by electrons and nuclei over point charges using
    !! the Ewald method.
    use alloc         , only : re_alloc, de_alloc
    use functions     , only : vec_norm
    use precision     , only : dp, grid_p
    use qmmm_neighbour, only : num_mmvecs, grid_veclist, grid_nr
    use qmmm_pbc      , only : reccel, pbc_displ_vector
    use sys           , only : die

    implicit none
    integer     , intent(in) :: na_qm
      !! Number of QM atoms.
    integer     , intent(in) :: na_mm
      !! Number of MM atoms.
    integer     , intent(in) :: natot
      !! Total (QM+MM) number of atoms.
    integer     , intent(in) :: ntpl
      !! Total number of grid points.
    integer     , intent(in) :: ntm(3)
      !! Number of mesh points along each axis.
    real(dp)    , intent(in) :: r(3,natot)
      !! Atomic positions.
    real(dp)    , intent(in) :: pc(na_mm)
      !! Atomic (classical) partial charges.
    real(dp)    , intent(in) :: ucell(3,3)
      !! Unit cell vectors.
    real(dp)    , intent(in) :: rcut_rho
      !! Cut-off radius grid point-charge interactions.
    real(dp)    , intent(in) :: dvol
      !! Volume of a point of the unit cell.
    real(dp)    , intent(in) :: ewald_alpha
      !! Ewald alpha factor.
    real(dp)    , intent(in) :: kewald_cutoff
      !! Ewald coefficient in the reciprocal space.
    real(grid_p), intent(in) :: Rho(ntpl)
      !! Electron density in the grid.
    character   , intent(in) :: lattice_type
      !! Type of cell lattice.
    real(dp)    , intent(inout) :: f(3,natot)
      !! Atomic forces.
    real(dp)    , intent(inout) :: stress(3,3)
      !! Cell stress.

    integer  :: iat, icrd, ivec, imesh, ix, iy, iz, js, n1, n2, n3, n1max, &
                n2max, n3max
    real(dp) :: rcut_rho2, kcell(3,3), dist, dist2, krecip(3), kronij, twopi, &
                kr, lattice_volume, drij(3), kmod2, kcut2, qm_ewald_alpha,    &
                qm_sqrt_ewald_alpha, stress_fact, dE, xm, ym, zm, const1,     &
                const2, const3, const4, const6, const7, qm_kewald_cutoff

    integer , parameter :: ewald_nmax = 20
    real(dp), pointer   :: S_qm_real(:,:,:), S_qm_imag(:,:,:), &
                           S_mm_real(:,:,:), S_mm_imag(:,:,:)

    if ( num_mmvecs == 0 ) return
    rcut_rho2      = ( rcut_rho / 0.529177_dp ) ** 2
    lattice_volume = dVol * ( real( ntpl, kind=dp ) )
    stress_fact    = 1.0_dp / lattice_volume

    ! For now, we put a range of orbital equals to rmax0 (bohrs)
    qm_kewald_cutoff    = kewald_cutoff * 0.529177_dp
    qm_ewald_alpha      = ewald_alpha   * 0.529177_dp * 0.529177_dp
    qm_sqrt_ewald_alpha = sqrt( qm_ewald_alpha )

    twopi  = 2.0_dp * acos( -1.0_dp )
    const1 = qm_sqrt_ewald_alpha / sqrt( 0.5_dp * twopi )
    const2 = 2.0_dp * twopi / lattice_volume

    ! We calculate the reciprocal lattice vectors
    call reccel( 3, ucell, kcell, 0 )

    drij(:) = kcell(1,:)
    n1max = INT( qm_kewald_cutoff / ( twopi * vec_norm( drij, 3) ) )
    if ( n1max > ewald_nmax ) call die( 'mmforce_ewald: n1max > ewald_nmax' )

    drij(:) = kcell(2,:)
    n2max = INT( qm_kewald_cutoff / ( twopi * vec_norm( drij, 3) ) )
    if ( n2max > ewald_nmax ) call die( 'mmforce_ewald: n2max > ewald_nmax' )

    drij(:) = kcell(3,:)
    n3max = INT( qm_kewald_cutoff / ( twopi * vec_norm( drij, 3) ) )
    if ( n3max > ewald_nmax ) call die( 'mmforce_ewald: n3max > ewald_nmax' )

    kcut2 = qm_kewald_cutoff * qm_kewald_cutoff

    nullify( S_qm_real, S_qm_imag, S_mm_real, S_mm_imag )
    call re_alloc( S_qm_real, -ewald_nmax, ewald_nmax, -ewald_nmax, ewald_nmax,&
                   -ewald_nmax, ewald_nmax, 'S_qm_real', 'mmforce_ewald' )
    call re_alloc( S_qm_imag, -ewald_nmax, ewald_nmax, -ewald_nmax, ewald_nmax,&
                   -ewald_nmax, ewald_nmax, 'S_qm_imag', 'mmforce_ewald' )
    call re_alloc( S_mm_real, -ewald_nmax, ewald_nmax, -ewald_nmax, ewald_nmax,&
                   -ewald_nmax, ewald_nmax, 'S_mm_real', 'mmforce_ewald' )
    call re_alloc( S_mm_imag, -ewald_nmax, ewald_nmax, -ewald_nmax, ewald_nmax,&
                   -ewald_nmax, ewald_nmax, 'S_mm_imag', 'mmforce_ewald' )

    ! This is done to avoid OpenMP Warnings.
    const3 = 0_dp; const7 = 0_dp; dist2 = 0_dp
    dist   = 0_dp; kmod2  = 0_dp; kr    = 0_dp
    xm     = 0_dp; ym     = 0_dp; zm    = 0_dp
    de     = 0_dp; kronij = 0_dp; imesh =    0
    js     = 0

    ! Real part of the Ewald summation
    ! Loop over the mesh points
    do iz = 0, ntm(3) -1
    do iy = 0, ntm(2) -1
    do ix = 0, ntm(1) -1
      imesh = 1 + ix + ntm(1) * iy + ntm(1) * ntm(2) * iz

      ! We use only grid points where there is density of charge.
      if ( abs(Rho(imesh)) > 0.0_dp ) then
        xm = ucell(1,1) * ix / ntm(1) + ucell(1,2) * iy / ntm(2) &
           + ucell(1,3) * iz / ntm(3)
        ym = ucell(2,1) * ix / ntm(1) + ucell(2,2) * iy / ntm(2) &
           + ucell(2,3) * iz / ntm(3)
        zm = ucell(3,1) * ix / ntm(1) + ucell(3,2) * iy / ntm(2) &
           + ucell(3,3) * iz / ntm(3)

        ! Loop over MM atoms
        do iat = 1, num_mmvecs
          js = grid_veclist(iat)

          if ( lattice_type == 'D' ) then
            drij(1) = xm - r(1,js) + grid_nr(1,iat) * ucell(1,1)
            drij(2) = ym - r(2,js) + grid_nr(2,iat) * ucell(2,2)
            drij(3) = zm - r(3,js) + grid_nr(3,iat) * ucell(3,3)
          else
            drij(1) = xm - r(1,js)
            drij(2) = ym - r(2,js)
            drij(3) = zm - r(3,js)

            do iCrd = 1, 3
            do iVec = 1, 3
              drij(iCrd) = drij(iCrd) + grid_nr(iVec,iat) * ucell(iCrd,iVec)
            enddo
            enddo
          endif
          call pbc_displ_vector( lattice_type, ucell, kcell, &
                                 drij(1), drij(2), drij(3) )
          dist2 = drij(1) * drij(1) + drij(2) * drij(2) + drij(3) * drij(3)


          ! Forces exerted on point charges by electrons.
          De = 0.0_dp
          if ( dist2 > rcut_rho2 ) then
            ! If our density point and point charge are too close,
            ! this might diverge. Thus, we avoid it.
            dist = sqrt( dist2 )
            De   = -2.0_dp * dvol * Rho(imesh) * pc(js-na_qm) / dist2 * &
                   ( 2.0_dp * const1 * exp( -qm_ewald_alpha * dist2 ) + &
                     erfc( qm_sqrt_ewald_alpha * dist ) / dist )
          endif

          do ivec = 1, 3
            f(ivec,js) = f(ivec,js) - De * drij(ivec)

            do icrd = 1, 3
              stress(icrd,ivec) = stress(icrd,ivec) &
                                - stress_fact * r(icrd,js) * De * drij(ivec)
            enddo
          enddo
        enddo ! at sv
      endif   ! abs(Rho(imesh)) > 0
    enddo     ! grid X
    enddo     ! grid Y
    enddo     ! grid Z

    ! Reciprocal space part of the ewald summation.
    ! Calculate structure factors for QM atoms.
    ! We first initialize and then loop over mesh points.
    S_qm_real(:,:,:) = 0.0_dp
    S_qm_imag(:,:,:) = 0.0_dp

!$OMP PARALLEL DEFAULT(FIRSTPRIVATE) SHARED(S_qm_real,S_qm_imag,Rho) &
!$OMP& PRIVATE(imesh)
!$OMP DO SCHEDULE(DYNAMIC,2)
    do iz = 0, ntm(3) -1
    do iy = 0, ntm(2) -1
    do ix = 0, ntm(1) -1
      imesh = 1 + ix + ntm(1) * iy + ntm(1) * ntm(2) * iz

      ! We use only grid points where there is density of charge.
      if ( abs(Rho(imesh)) > 0.0_dp ) then
        xm = ucell(1,1) * ix / ntm(1) + ucell(1,2) * iy / ntm(2) &
           + ucell(1,3) * iz / ntm(3)
        ym = ucell(2,1) * ix / ntm(1) + ucell(2,2) * iy / ntm(2) &
           + ucell(2,3) * iz / ntm(3)
        zm = ucell(3,1) * ix / ntm(1) + ucell(3,2) * iy / ntm(2) &
           + ucell(3,3) * iz / ntm(3)

        !  Reciprocal-space sum, depends on the lattice.
        if ( lattice_type == 'D' ) then
          do n1 = -n1max, n1max
          do n2 = -n2max, n2max
          do n3 = -n3max, n3max
            if ( (n1 == 0) .and. (n2 == 0) .and. (n3 == 0) ) cycle

            krecip(1) = n1 * twopi * kcell(1,1)
            krecip(2) = n2 * twopi * kcell(2,2)
            krecip(3) = n3 * twopi * kcell(3,3)
            kmod2 = krecip(1) * krecip(1) + krecip(2) * krecip(2) &
                  + krecip(3) * krecip(3)

            if ( kmod2 > kcut2 ) cycle
            kr = krecip(1) * xm + krecip(2) * ym + krecip(3) * zm
            S_qm_real(n1,n2,n3) = S_qm_real(n1,n2,n3) + &
                                  2.0_dp * dvol * Rho(imesh) * cos(kr)
            S_qm_imag(n1,n2,n3) = S_qm_imag(n1,n2,n3) + &
                                  2.0_dp * dvol * Rho(imesh) * sin(kr)
          enddo
          enddo
          enddo
        else
          ! Reciprocal-space sum
          do n1 = -n1max, n1max
          do n2 = -n2max, n2max
          do n3 = -n3max, n3max
            if ( (n1 == 0) .and. (n2 == 0) .and. (n3 == 0) ) cycle

            krecip(1) = twopi * ( n1 * kcell(1,1) + n2 * kcell(2,1) &
                                + n3 * kcell(3,1) )
            krecip(2) = twopi * ( n1 * kcell(1,2) + n2 * kcell(2,2) &
                                + n3 * kcell(3,2) )
            krecip(3) = twopi * ( n1 * kcell(1,3) + n2 * kcell(2,3) &
                                + n3 * kcell(3,3) )
            kmod2 = krecip(1) * krecip(1) + krecip(2) * krecip(2) &
                  + krecip(3) * krecip(3)

            if ( kmod2 > kcut2 ) cycle
            kr = krecip(1) * xm + krecip(2) * ym + krecip(3) * zm
            S_qm_real(n1,n2,n3) = S_qm_real(n1,n2,n3) + &
                                  2.0_dp * dvol * Rho(imesh) * cos(kr)
            S_qm_imag(n1,n2,n3) = S_qm_imag(n1,n2,n3) + &
                                  2.0_dp * dvol * Rho(imesh) * sin(kr)
          enddo
          enddo
          enddo
        endif ! Lattice type
      endif   ! abs(Rho(imesh)) > 0
    enddo     ! grid X
    enddo     ! grid Y
    enddo     ! grid Z
!$OMP END DO
!$OMP END PARALLEL

    ! Calculate structure factors for all MM atoms.
    S_mm_real(:,:,:) = 0.0_dp
    S_mm_imag(:,:,:) = 0.0_dp

    ! Reciprocal-space sum depends on the lattice type.
    if ( lattice_type == 'D' ) then
!$OMP   PARALLEL DEFAULT(FIRSTPRIVATE) SHARED(S_mm_real,S_mm_imag,Rho)
!$OMP DO SCHEDULE(DYNAMIC,2)
      do n1 = -n1max, n1max
      do n2 = -n2max, n2max
      do n3 = -n3max, n3max
        if ( (n1 == 0) .and. (n2 == 0) .and. (n3 == 0) ) cycle

        ! Loop over MM atoms
        krecip(1) = n1 * twopi * kcell(1,1)
        krecip(2) = n2 * twopi * kcell(2,2)
        krecip(3) = n3 * twopi * kcell(3,3)
        kmod2 = krecip(1) * krecip(1) + krecip(2) * krecip(2) &
              + krecip(3) * krecip(3)

        if ( kmod2 > kcut2 ) cycle

        ! Loop over MM atoms
        do iat = na_qm+1, natot
          kr = krecip(1) * r(1,iat) + krecip(2) * r(2,iat) &
             + krecip(3) * r(3,iat)
          S_mm_real(n1,n2,n3) = S_mm_real(n1,n2,n3) + pc(iat-na_qm) * cos(kr)
          S_mm_imag(n1,n2,n3) = S_mm_imag(n1,n2,n3) + pc(iat-na_qm) * sin(kr)
        enddo
      enddo
      enddo
      enddo
!$OMP END DO
!$OMP END PARALLEL
    else ! Lattice
!$OMP PARALLEL DEFAULT(FIRSTPRIVATE)  SHARED(S_mm_real,S_mm_imag,Rho)
!$OMP DO SCHEDULE(DYNAMIC,2)
      do n1 = -n1max, n1max
      do n2 = -n2max, n2max
      do n3 = -n3max, n3max
        if ( (n1 == 0) .and. (n2 == 0) .and. (n3 == 0) ) cycle

        krecip(1) = twopi * ( n1 * kcell(1,1) + n2 * kcell(2,1) &
                            + n3 * kcell(3,1) )
        krecip(2) = twopi * ( n1 * kcell(1,2) + n2 * kcell(2,2) &
                            + n3 * kcell(3,2) )
        krecip(3) = twopi * ( n1 * kcell(1,3) + n2 * kcell(2,3) &
                            + n3 * kcell(3,3) )
        kmod2 = krecip(1) * krecip(1) + krecip(2) * krecip(2) &
              + krecip(3) * krecip(3)

        if ( kmod2 > kcut2 ) cycle

        ! Loop over MM atoms
        do iat = na_qm+1, natot
          kr = krecip(1) * r(1,iat) + krecip(2) * r(2,iat) &
             + krecip(3) * r(3,iat)
          S_mm_real(n1,n2,n3) = S_mm_real(n1,n2,n3) + pc(iat-na_qm) * cos(kr)
          S_mm_imag(n1,n2,n3) = S_mm_imag(n1,n2,n3) + pc(iat-na_qm) * sin(kr)
        enddo
      enddo
      enddo
      enddo
!$OMP END DO
!$OMP END PARALLEL
    endif ! Lattice

    const4 = 1.0_dp / lattice_volume
    const6 = 1.0_dp / ( 4.0_dp * ewald_alpha )

    ! Calculate the reciprocal part of the force on classical atoms
    ! due to QM grid points
    if ( lattice_type == 'D' ) then
      do n1 = -n1max, n1max
      do n2 = -n2max, n2max
      do n3 = -n3max, n3max
        if ( (n1 == 0) .and. (n2 == 0) .and. (n3 == 0) ) cycle

        ! Loop over MM atoms
        krecip(1) = n1 * twopi * kcell(1,1)
        krecip(2) = n2 * twopi * kcell(2,2)
        krecip(3) = n3 * twopi * kcell(3,3)
        kmod2 = krecip(1) * krecip(1) + krecip(2) * krecip(2) &
              + krecip(3) * krecip(3)

        if ( kmod2 > kcut2 ) cycle
        const3 = ( const2 / kmod2 ) * exp( -kmod2 / (4.0_dp * qm_ewald_alpha) )

        ! Loop over MM atoms.
        do iat = na_qm +1, natot
          kr = krecip(1) * r(1,iat) + krecip(2) * r(2,iat) &
             + krecip(3) * r(3,iat)
          De = const3 * pc(iat-na_qm) * ( cos(kr) * S_qm_imag(n1,n2,n3) &
                                        - sin(kr) * S_qm_real(n1,n2,n3) )
          do iCrd = 1, 3
            f(iCrd,iat) = f(iCrd,iat) + De * krecip(iCrd)
          enddo
        enddo

        De = const3 * const4 * ( S_mm_imag(n1,n2,n3) * S_qm_imag(n1,n2,n3) &
                               + S_mm_real(n1,n2,n3) * S_qm_real(n1,n2,n3) )
        const7 = 2.0_dp * ( 1.0_dp + kmod2 * const6 ) / kmod2
        do iVec = 1, 3
        do iCrd = 1, 3
          kronij = real( int( ( iCrd + iVec - abs( iCrd - iVec ) ) &
                            / ( iCrd + iVec + abs( iCrd - iVec ) ) ), kind=dp )
          stress(iCrd,iVec) = stress(iCrd,iVec) + De * &
                              ( kronij - const7 * krecip(iCrd) * krecip(iVec) )
        enddo
        enddo
      enddo
      enddo
      enddo
    else ! Lattice
      do n1 = -n1max, n1max
      do n2 = -n2max, n2max
      do n3 = -n3max, n3max
        if ( (n1 == 0) .and. (n2 == 0) .and. (n3 == 0) ) cycle

        krecip(1) = twopi * ( n1 * kcell(1,1) + n2 * kcell(2,1) &
                            + n3 * kcell(3,1) )
        krecip(2) = twopi * ( n1 * kcell(1,2) + n2 * kcell(2,2) &
                            + n3 * kcell(3,2) )
        krecip(3) = twopi * ( n1 * kcell(1,3) + n2 * kcell(2,3) &
                            + n3 * kcell(3,3) )
        kmod2 = krecip(1) * krecip(1) + krecip(2) * krecip(2) &
              + krecip(3) * krecip(3)

        if ( kmod2 > kcut2 ) cycle
        const3 = ( const2 / kmod2 ) * exp( -kmod2 / (4.0_dp * qm_ewald_alpha) )

        ! Loop over MM atoms.
        do iat = na_qm +1, natot
          kr = krecip(1) * r(1,iat) + krecip(2) * r(2,iat) &
             + krecip(3) * r(3,iat)
          De = const3 * pc(iat-na_qm) * ( cos(kr) * S_qm_imag(n1,n2,n3) &
                                        - sin(kr) * S_qm_real(n1,n2,n3) )
          do iCrd = 1, 3
            f(iCrd,iat) = f(iCrd,iat) + De * krecip(iCrd)
          enddo
        enddo

        De = const3 * const4 * ( S_mm_imag(n1,n2,n3) * S_qm_imag(n1,n2,n3) &
                               + S_mm_real(n1,n2,n3) * S_qm_real(n1,n2,n3) )
        const7 = 2.0_dp * ( 1.0_dp + kmod2 * const6 ) / kmod2
        do iVec = 1, 3
        do iCrd = 1, 3
          kronij = real( int( ( iCrd + iVec - abs( iCrd - iVec ) ) &
                            / ( iCrd + iVec + abs( iCrd - iVec ) ) ), kind=dp )
          stress(iCrd,iVec) = stress(iCrd,iVec) + De * &
                              ( kronij - const7 * krecip(iCrd) * krecip(iVec) )
        enddo
        enddo
      enddo
      enddo
      enddo
    endif ! Lattice

    call de_alloc( S_qm_real, 'S_qm_real', 'mmforce_ewald' )
    call de_alloc( S_qm_imag, 'S_qm_imag', 'mmforce_ewald' )
    call de_alloc( S_mm_real, 'S_mm_real', 'mmforce_ewald' )
    call de_alloc( S_mm_imag, 'S_mm_imag', 'mmforce_ewald' )
  end subroutine mmforce_ewald
end module mmforces_m
