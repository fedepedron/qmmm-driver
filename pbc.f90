module qmmm_pbc
  !! Subroutines used to deal with the PBC
  implicit none
  public :: reinserting_atoms_in_box
  public :: pbc_displ_vector
  public :: get_pbc_vectors
  public :: get_lattice_type
  public :: reccel

contains

  subroutine reinserting_atoms_in_box( lattice_type, natot, na_qm, nac, &
                                       noaa, rclas, cell )

    use precision, only: dp

    implicit none
    character, intent(in)    :: lattice_type
    integer  , intent(in)    :: natot
    integer  , intent(in)    :: na_qm
    integer  , intent(in)    :: nac
    real(dp) , intent(inout) :: rclas(3,natot)
    real(dp) , intent(in)    :: cell(3,3)

    character(len=4) :: noaa(nac)
    integer  :: iCrd, iat, jWat, kCrd, nk, shift
    real(dp) :: rnk, kcell(3,3)


    iat = 0
    shift = 0

    if ( lattice_type == 'D' ) then
      do
        iat = iat + shift +1

        if ( iat > natot ) exit

        shift = 0
        if ( iat > na_qm ) then
          if ( noaa(iat-na_qm) == 'HOH' ) shift = 2
        endif

         do iCrd = 1, 3
           if ( rclas(iCrd,iat) < 0.0_dp ) then
             do jWat = 0, shift
               rclas(iCrd,iat+jWat) = rclas(iCrd,iat+jWat) + cell(iCrd,iCrd)
             enddo
           elseif ( rclas(iCrd,iat) > cell(iCrd,iCrd) ) then
             do jWat = 0, shift
               rclas(iCrd,iat+jWat) = rclas(iCrd,iat+jWat) - cell(iCrd,iCrd)
             enddo
           endif
         enddo
      enddo

    else
      call reccel( 3, cell, kcell, 0 )

      do
        iat = iat + shift +1
        if ( iat > natot ) exit
        shift = 0

        if ( iat > na_qm ) then
          if ( noaa(iat-na_qm) == 'HOH' ) shift = 2
        endif

        do kCrd = 1, 3
          rnk = 0.0_dp

          ! Here it is assumed that kcell is the matrix, whose rows are
          ! the reciprocal vectors without 2*pi factor.
          do iCrd = 1, 3
            rnk = rnk + rclas(kCrd,iat) * kcell(iCrd,kCrd)
          enddo

          if ( rnk >= 0.0_dp ) then
            nk = INT(rnk)
          else
            nk = NINT(rnk-0.5_dp)
          endif

          if ( nk /= 0 ) then
            do jWat = 0, shift
            do iCrd = 1, 3
              rclas(kCrd,iat+jWat) = rclas(kCrd,iat+jWat) - cell(iCrd,kCrd) * nk
            enddo
            enddo
          endif
        enddo
      enddo
    endif

  end subroutine reinserting_atoms_in_box

      subroutine pbc_displ_vector(lattice_type,cell,kcell,dx,dy,dz)

      use precision, only: dp
      implicit none

      character lattice_type
      real(dp) dx, dy, dz
      real(dp) dr(3)
      real(dp) cell(3,3), kcell(3,3)

      integer k, l, nk

      dr(1)=dx
      dr(2)=dy
      dr(3)=dz

      if (lattice_type=='D') then

         do k=1,3
            dr(k) = dr(k) - ANINT(dr(k)/cell(k,k))*cell(k,k)
         enddo

      else

         do k=1,3
!     Here it is assumed that kcell is matrix of the reciprocal vectors
!     without 2*pi factor
            nk=ANINT(dr(1)*kcell(k,1)+dr(2)*kcell(k,2)+dr(3)*kcell(k,3))
            if (nk.ne.0) then
               do l=1,3
                  dr(k)=dr(k)-cell(l,k)*nk
               enddo
            endif
         enddo

      endif

      dx=dr(1)
      dy=dr(2)
      dz=dr(3)

      end subroutine pbc_displ_vector

      subroutine get_pbc_vectors(lattice_type,cell,kcell,drij,nr)

      use precision, only: dp
      implicit none

      character lattice_type
      real(dp) cell(3,3), kcell(3,3)
      real(dp) drij(3)
      integer nr(3)

      integer k

      if (lattice_type=='D') then
         do k=1,3
            nr(k) = -ANINT(drij(k)/cell(k,k))
         enddo
      else
         do k=1,3
!     Here it is assumed that kcell is matrix of the reciprocal vectors
!     without 2*pi factor
            nr(k)=-ANINT(drij(1)*kcell(k,1)+ &
                 drij(2)*kcell(k,2)+drij(3)*kcell(k,3))
         enddo
      endif

      end subroutine get_pbc_vectors

      character function get_lattice_type(cell)

       use precision, only: dp
       implicit none

! RETURN 'D' IF THE LATTICE VECTORS LIE ALONG THE X-, Y-
! AND Z-AXIS RESPECTIVELY. IN OTHER WORDS, THE CELL MATRIX IS DIAGONAL.
! RETURN 'G' OTHERWISE.

        real(dp) cell(3,3)
        integer i, j

        get_lattice_type='D'

        do i=1,3
           do j=1,3
              if (i.ne.j) then
                 if (cell(i,j).ne.0.0) get_lattice_type='G'
              endif
           enddo
        enddo

      end function get_lattice_type

      subroutine reccel( N, A, B, IOPT )

        use precision, only: dp
        use sys, only: die

        implicit none

!  CALCULATES RECIPROCAL LATTICE VECTORS B.
!  THEIR PRODUCT WITH DIRECT LATTICE VECTORS A IS 1 (IF IOPT=0) OR
!  2*PI (IF IOPT=1). N IS THE SPACE DIMENSION.
!  WRITTEN BY J.M.SOLER.

      integer :: n, iopt
      real(dp):: A(N,N),B(N,N), c, ci

      integer :: i

      C=1.D0
      IF (IOPT.EQ.1) C=2.D0*ACOS(-1.D0)

      IF (N .EQ. 1) THEN
        B(1,1) = C / A(1,1)
      ELSEIF (N .EQ. 2) THEN
        C = C / (A(1,1)*A(2,2) - A(1,2)*A(2,1))
        B(1,1) =  A(2,2)*C
        B(1,2) = (-A(2,1))*C
        B(2,1) = (-A(1,2))*C
        B(2,2) =  A(1,1)*C
      ELSEIF (N .EQ. 3) THEN
        B(1,1)=A(2,2)*A(3,3)-A(3,2)*A(2,3)
        B(2,1)=A(3,2)*A(1,3)-A(1,2)*A(3,3)
        B(3,1)=A(1,2)*A(2,3)-A(2,2)*A(1,3)
        B(1,2)=A(2,3)*A(3,1)-A(3,3)*A(2,1)
        B(2,2)=A(3,3)*A(1,1)-A(1,3)*A(3,1)
        B(3,2)=A(1,3)*A(2,1)-A(2,3)*A(1,1)
        B(1,3)=A(2,1)*A(3,2)-A(3,1)*A(2,2)
        B(2,3)=A(3,1)*A(1,2)-A(1,1)*A(3,2)
        B(3,3)=A(1,1)*A(2,2)-A(2,1)*A(1,2)
        DO 20 I=1,3
          CI=C/(A(1,I)*B(1,I)+A(2,I)*B(2,I)+A(3,I)*B(3,I))
          B(1,I)=B(1,I)*CI
          B(2,I)=B(2,I)*CI
          B(3,I)=B(3,I)*CI
  20    CONTINUE
      ELSE
         call die('RECCEL: NOT PREPARED FOR N>3')
      ENDIF

      end subroutine reccel
end module qmmm_pbc