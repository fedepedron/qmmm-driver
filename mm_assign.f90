module mm_assign_m
  !! This module handles the assignation of classical parameters
  !! for the MM regions.
  ! This is basically a reformat of the old "solv_assign" subroutine.
  implicit none
  public :: mm_assign

  private
  integer, parameter :: MAX_NUM_RES = 20000
    !! Maximum number of residues.
  integer, parameter :: MAX_TYPE_RES = 200
    !! Maximum number of different residue types.
  integer, parameter :: MAX_CONNEC = 1000
    !! Maximum number of connectivities between MM atoms.

contains
  subroutine mm_assign( na_qm, na_mm, nroaa, Em, Rm, attype, pc, ng1,        &
                        bondxat, angexat, atange, angmxat, atangm, dihexat,  &
                        atdihe, dihmxat, atdihm, impxat, atimp, nbond, kbond,&
                        bondeq, bondtype, nangle, kangle, angleeq, angletype,&
                        ndihe, kdihe, diheeq, dihetype, multidihe, perdihe,  &
                        nimp, kimp, impeq, imptype, multiimp, perimp, nparm, &
                        aaname, atname, aanum, qmattype, rmm,                &
                        graphite_layer_no, rcut_qmmm, rcut_qm, rcut_mm, sfc, &
                        radbloqmmm, atsinres, mmcell, lattice_type )
    !! Assigns MM parameters.
    use fdf       , only : fdf_boolean
    use graphite_m, only : find_graphite_layers
    use m_qmmm_fdf, only : fdf_block_qmmm
    use precision , only : dp
    use qmmm_pbc  , only : get_lattice_type
    use sys       , only : die

    implicit none
    integer          , intent(in)  :: na_qm
      !! Number of QM atoms.
    integer          , intent(in)  :: na_mm
      !! Number of MM atoms.
    integer          , intent(in)  :: nparm
      !! Maximum number of parameters. The size of all parameter arrays.
    integer          , intent(out)   :: nroaa
      !! Number of classical residues.

    real(dp)         , intent(out)   :: Em(na_qm+na_mm)
      !! Lennard-Jones epsilon.
    real(dp)         , intent(out)   :: Rm(na_qm+na_mm)
      !! Lennard-Jones Rmin.
    character(len=4) , intent(out)   :: attype(na_mm)
      !! MM atom types.
    real(dp)         , intent(out)   :: pc(na_mm)
      !! Partial classical charges for MM atoms.
    integer          , intent(out)   :: ng1(na_mm,6)
      !! First neighbours for the purpose of AMBER connectivity.

    integer          , intent(out)   :: bondxat(na_mm)
      !! Number of bonds involving a given atom.
    integer          , intent(out)   :: angexat(na_mm)
      !! Number of angles involving a given atom in the endpoints.
    integer          , intent(out)   :: angmxat(na_mm)
      !! Number of angles involving a given atom in the middle.
    integer          , intent(out)   :: atange(na_mm,25,2)
      !! For the angles in angexat, the indices of neighbours involved.
    integer          , intent(out)   :: atangm(na_mm,25,2)
      !! For the angles in angmxat, the indices of neighbours involved.
    integer          , intent(out)   :: dihexat(na_mm)
      !! Number of dihedrals involving a given atom in the endpoints.
    integer          , intent(out)   :: dihmxat(na_mm)
      !! Number of dihedrals involving a given atom in the middle.
    integer          , intent(out)   :: atdihe(na_mm,100,3)
      !! For the angles in dihexat, the indices of neighbours involved.
    integer          , intent(out)   :: atdihm(na_mm,100,3)
      !! For the angles in dihmxat, the indices of neighbours involved.
    integer          , intent(out)   :: impxat(na_mm)
      !! Number of improper angles involving a given atom in the endpoints.
    integer          , intent(out)   :: atimp(na_mm,25,4)
      !! For the improper angles in impxat, the indices of neighbours involved.

    integer          , intent(out)   :: nbond
      !! Total number of bonds.
    real(dp)         , intent(out)   :: kbond(nparm)
      !! List of bond constants.
    real(dp)         , intent(out)   :: bondeq(nparm)
      !! List of bond equilibrium distances.
    character(len=5) , intent(out)   :: bondtype(nparm)
      !! Bond types.

    integer          , intent(out)   :: nangle
      !! Total number of angles.
    real(dp)         , intent(out)   :: kangle(nparm)
      !! List of angle constants.
    real(dp)         , intent(out)   :: angleeq(nparm)
      !! List of angle equilibrium positions.
    character(len=8) , intent(out)   :: angletype(nparm)
      !! Angle types.

    integer          , intent(out)   :: ndihe
      !! Total number of dihedral angles.
    real(dp)         , intent(out)   :: kdihe(nparm)
      !! List of dihedral angle constants.
    real(dp)         , intent(out)   :: diheeq(nparm)
      !! List of dihedral angle equilibrium positions.
    character(len=11), intent(out)   :: dihetype(nparm)
      !! Dihedral angle types.
    integer          , intent(out)   :: multidihe(nparm)
      !! Dihedral angle multiplicity (in 360°).
    real(dp)         , intent(out)   :: perdihe(nparm)
      !! Dihedral angle periodicity (angle value).

    integer          , intent(out)   :: nimp
      !! Total number of improper angles.
    real(dp)         , intent(out)   :: kimp(nparm)
      !! List of improper angle constants.
    real(dp)         , intent(out)   :: impeq(nparm)
      !! List of improper angle equilibrium positions.
    character(len=11), intent(out)   :: imptype(nparm)
      !! Improper angle types.
    integer          , intent(out)   :: multiimp(nparm)
      !! Improper angle multiplicity (in 360°).
    real(dp)         , intent(out)   :: perimp(nparm)
      !! Improper angle periodicity (angle value).

    character(len=4) , intent(out)   :: aaname(na_mm)
      !! Residue names.
    character(len=4) , intent(out)   :: atname(na_mm)
      !! Atom names.
    integer          , intent(out)   :: aanum(na_mm)
      !! Index of the residue to whom an atom belongs.
    character(len=4) , intent(out)   :: qmattype(na_qm)
      !! Classical atom types for the QM regions. Used for LJ.
    real(dp)         , intent(out)   :: rmm(3,na_mm)
      !! Positions of the MM atoms.
    integer          , intent(out)   :: graphite_layer_no(na_mm)
      !! For graphite layering, the layer corresponding to each MM atom.

    real(dp)         , intent(out)   :: rcut_qmmm
      !! Distance cut-off for QM-MM interactions.
    real(dp)         , intent(out)   :: rcut_mm
      !! Distance cut-off for MM-MM interactions.
    real(dp)         , intent(out)   :: rcut_qm
      !! Proximity cut-off between points of the density grid and MM positions.
    real(dp)         , intent(out)   :: sfc
      !! Smoothing function cut-off. It is technically constant...
    real(dp)         , intent(out)   :: radbloqmmm
      !! Freeze all MM atoms beyond this distance from the QM region.

    integer          , intent(out)   :: atsinres(MAX_NUM_RES)
      !! Number of atoms in a given residue.
    real(dp)         , intent(inout) :: mmcell(3,3)
      !! Periodic cell vectors.
    character(len=1) , intent(out)   :: lattice_type
      !! Type of PBC lattice.

    character(len=1) :: ch1
    character(len=4) :: ch4
    integer          :: iat, jat, ios, idumm, ires, jres, nres, iconn, &
                        iunit, ncon, ivec
    logical          :: foundamber, debug
    real(dp)         :: rcut, xmin, xmax
    character(len=4), allocatable :: resname(:), atnamea(:,:), attypea(:,:), &
                                     aanamea(:)
    integer         , allocatable :: atnu(:,:), nataa(:,:), con(:,:)   , &
                                     con2(:,:), atxres(:) , atomsxaa(:), &
                                     resnum(:)

    ! Externals
    real(dp) , external :: volcel

    ! Initialize variables.
    rmm   = 0.0_dp ; aanum  = 0 ; nroaa = 0 ; ng1  = 0
    nbond = 0      ; nangle = 0 ; ndihe = 0 ; nimp = 0
    sfc   = 2.0_dp ; ncon   = 0 ; ios   = 0

    rcut_mm = 100.0_dp  ; rcut_qmmm  = 100.0_dp
    rcut_qm = 1.E-06_dp ; radbloqmmm = 100.0_dp

    foundamber=.false.
    debug = fdf_boolean( 'SolventDebug' , .false. )

    if ( debug ) then
      write( 6, '(/,a,70(1h=))' ) 'mm_assign: '
      write( 6, "(a)" ) 'mm_assign: Debug mode enabled.'
      write( 6, "(a)" ) 'mm_assign: Remember to check input and amber.parm.'
    endif

    ! Read MM coordinates by atom
    allocate( resnum(na_mm) )
    if ( fdf_block_qmmm('SolventInput',iunit) ) then
      do iat = 1, na_mm
        read( iunit, iostat = ios, fmt = '(A4,I7,2x,A4,A4,A,I4,4x,3f8.3)' ) &
          ch4, idumm, atname(iat), aaname(iat), ch1, resnum(iat), rmm(1:3,iat)
        if ( ios /= 0 ) &
          call die( 'mm_assign: Problem while reading MM coordinates.')
      enddo
    else
      write( 6, "(a)" ) 'mm_assign: SolventInput block not found.'
      call die( "mm_assign: You must specify the MM atom coordinates." )
    endif

    ! Change coordinates to SIESTA units
    rmm(1:3,1:na_mm) = rmm(1:3,1:na_mm) / 0.529177_dp

    ! Read cutoff radii for different sections of the code.
    if ( fdf_block_qmmm( 'CutOffRadius', iunit ) ) then
      read( iunit, *, iostat = ios ) ch1, rcut_qm
      read( iunit, *, iostat = ios ) ch1, rcut_qmmm
      read( iunit, *, iostat = ios ) ch1, rcut_mm
      read( iunit, *, iostat = ios ) ch1, radbloqmmm

      if ( ios /= 0 ) call die( "mm_assign: Problems reading cut-off radii." )
    else
      write( 6, '(a)' ) "mm_assign: WARNING: CutOffRadius block not found."
      write( 6, '(a)' ) "mm_assign: WARNING: using defaults for cut-off radii."
    endif

    rcut = max( rcut_mm, rcut_qmmm )

    ! Read lattice vectors for periodic boundary conditions.
    ! We create a cubic cell if there is no previous cell.
    if ( abs(volcel( mmcell )) < 1.0e-8_dp ) then
      do ivec = 1, 3
        xmin = huge( 1.0_dp )
        xmax = -xmin

        do iat = 1, na_mm
          xmin = min( xmin, rmm(ivec,iat) - rcut )
          xmax = max( xmax, rmm(ivec,iat) + rcut )
        enddo

        mmcell(ivec,ivec) = xmax - xmin
      enddo
    endif

    lattice_type = get_lattice_type( mmcell )

    ! Assigns the number and indices of residues.
    ires = 1
    aanum(1) = ires
    do iat = 2, na_mm
      if ( resnum(iat) == resnum(iat-1) ) then
        aanum(iat) = aanum(iat-1)
      else
        ires = ires +1
        aanum(iat)= ires
      endif
    enddo
    deallocate( resnum )
    nroaa = aanum(na_mm)

    if ( nroaa == 0 ) &
      call die( "mm_assign: Number of residues cannot be zero." )

    if ( na_mm == 0 ) nroaa = 0

    ! Read QM atom types
    if ( na_qm /= 0 ) then
      if ( fdf_block_qmmm( 'SoluteAtomTypes', iunit ) ) then
        do iat = 1, na_qm +1
          read( iunit, *, iostat = ios ) ch4
          if ( ios /= 0 ) &
            call die( "mm_assign: Problems reading QM atom types." )

          ch1 = ch4(1:1)

          if ( iat == na_qm+1 ) then
            if ( ch1 == '%' ) exit
            call die( 'mm_assign: More QM atom types than na_qm.' )
          endif
          if ( ch1 == '%' ) &
            call die( 'mm_assign: Less QM atom types than na_qm.' )

          qmattype(iat) = ch4
        enddo
      else
        write( 6, "(a)" ) 'mm_assign: SoluteAtomTypes block not found.'
        call die('mm_assign: You must specify QM atom types.')
      endif
    endif

    ! Check the values for cut-off radii
    if ( rcut_qm < 1.e-8_dp ) &
      call die( 'mm_assign: QM grid cut-off radius too close to zero.' )
    if ( rcut_qmmm < 1.e-8_dp ) &
      call die( 'mm_assign: QM-MM cut-off radius too close to zero' )

    ! External solvent connectivity block
    allocate( con2(2, MAX_CONNEC) )
    if ( fdf_block_qmmm( 'SolventConnectivity', iunit ) ) then
      iconn = 1

      do while (.true.)
        if ( iconn > MAX_CONNEC ) &
          call die('mm_assign: Amount of MM connectivities > MAX_CONNEC.')

        read( iunit, '(a)', advance = 'no', iostat = ios ) ch1
        if ( ios /= 0 ) &
          call die( "mm_assign: Problems reading MM connectivities." )
        if ( ch1 == '%' ) exit

        read( iunit, '(a)', advance = 'no', iostat = ios ) ch1, con2(1:2,iconn)
        if ( ios /= 0 ) &
          call die( "mm_assign: Problems reading MM connectivities." )

        iconn = iconn +1
      enddo

      ncon = iconn -1

      allocate( con( 2, ncon ) )

      con( 1:2, 1:ncon ) = con2( 1:2, 1:ncon )

      if ( ncon /= 0 ) &
        write( 6, '(a)' ) 'mm_assign: Read new connectivities block.'
    else
      write( 6, '(a)' ) 'mm_assign: SolventConnectivity block not found.'
      ncon = 0
      allocate( con(2, ncon+1) )
    endif
    deallocate( con2 )
    if ( debug ) write( 6, '(a)' ) 'mm_assign: Done reading from input.'

    ! Now we need the parameters. First we check if the amber.parm file exists.
    inquire( file = "amber.parm", exist = foundamber )
    if ( .not. foundamber ) &
      call die( "mm_assign: 'amber.parm' file not found." )

    ! If found, we call the subroutine that reads the bond, angle, dihedral
    ! and improper angle parameters.
    call amber_union_parms( nbond, kbond, bondeq, bondtype, nangle, kangle, &
                            angleeq, angletype, ndihe, kdihe, diheeq,       &
                            dihetype, multidihe, perdihe, nimp, kimp, impeq,&
                            imptype, multiimp, perimp, nparm )
    if ( debug ) write( 6, '(a)' ) 'mm_assign: Read conectivity parameters.'

    ! Now we start the real assignation, according to each atom.
    allocate( atnu(nroaa,100), atnamea(nroaa,100), resname(nroaa), &
              atxres(nroaa) )

    ! Gets the amount of atoms per residue.
    allocate( atomsxaa(MAX_TYPE_RES), aanamea(MAX_TYPE_RES) )
    call atxaa( nres, aanamea, atomsxaa )

    jat = 1 ; atxres = 0
    do ires = 1, nroaa
      resname(ires) = aaname(jat)

      do jres = 1, nres
        if ( resname(ires) == aanamea(jres) ) atxres(ires) = atomsxaa(jres)
      enddo
      do iat = 1, atxres(ires)
        atnu(ires,iat)    = jat
        atnamea(ires,iat) = atname(jat)

        if ( aanum(atnu(ires,iat)) /= aanum(atnu(ires,1)) ) then
          write( 6, * ) 'mm_assign: Atom missing in residue: ', &
                        resname(ires), ires
          call die( 'mm_assign: MM atoms missing.' )
        endif

        jat = jat +1
      enddo

      if ( atxres(ires) == 0 ) then
        write( 6, * ) 'mm_assign: Wrong residue name  :', resname(ires), ires
        call die('mm_assign:  Wrong residue name(s).' )
      endif
    enddo
    deallocate( atomsxaa, aanamea )
    if ( debug ) write( 6, '(a)' ) 'mm_assign: Done with variable exchanges.'

    ! The following call extracts AMBER classical monoatomic parameters.
    allocate( attypea(nroaa,100), nataa(nroaa,100) )
    call paramats( nroaa, resname, atnamea, attypea, nataa, &
                   na_mm, atxres, pc, attype )
    if ( debug ) write( 6, '(a)' ) 'mm_assign: Called paramats.'

    ! And here we obtain Lennard-Jones parameters.
    call read_lj( nroaa, attypea, na_qm, na_mm, Em, Rm, qmattype, atxres )
    if ( debug ) write( 6, '(a)' ) 'mm_assign: Called lj().'

    ! We then assign first neighbours according to connectivity.
    call amber_ng1( na_mm, nataa, nroaa, atxres, atnu, resname, ng1, atnamea,&
                    ncon, con, rmm, mmcell, lattice_type )
    if ( debug ) then
      write( 6, '(a)' ) 'mm_assign: Done with first neighbours.'
      write( 6, '(a)' ) 'mm_assign: Connectivity matrix:'

      do iat = 1, na_mm
        write( 6, "('mm_assign: ',7I6)" ) iat, ng1(iat,1:6)
      enddo
    endif

    ! Here we calculate all connectivity magnitudes: bonds, angles, dihedrals
    ! and improper angles.
    call bon_ang_dih_imp( na_mm, ng1, atange, atangm, atdihe, atdihm, bondxat,&
                          angexat, angmxat, dihexat, dihmxat, atnamea, nroaa, &
                          atxres, atnu, resname, atimp, impxat )
    if ( debug ) &
      write( 6, '(a)' ) 'mm_assign: Calculated bonds, angles, dihedrals.'

    graphite_layer_no(1:na_mm)=0
    do ires = 1, nroaa
      if ( resname(ires) == 'GRAP' ) then
        call find_graphite_layers( na_mm, attype, ng1, bondxat, graphite_layer_no )
        exit
      endif
    enddo

    ! We convert the name WAT to HOH, for internal conventions.
    ! Then, we verify that water atoms are in the correct order (oxygen first).
    do iat = 1, na_mm
      if ( aaname(iat) == 'WAT' ) aaname(iat) = 'HOH'
    enddo
    do ires = 1, nroaa
      if ( resname(ires) == 'WAT' ) resname(ires) = 'HOH'
    enddo
    do ires = 1, nroaa
      if ( .not. (resname(ires) == 'HOH') ) cycle

      do iat = 1, atxres(ires)
        if (.not. (atnamea(ires,iat) == 'O') ) cycle
        if ( iat == 1) cycle

        write( 6, * ) 'mm_assign: Wrong atom order in water residue ', ires
        call die( 'Wrong atom ordering in water molecules' )
      enddo
    enddo

    ! Sets up atsinres array, the number of atoms in each residue.
    atsinres(:) = 0
    if ( nroaa > MAX_NUM_RES ) &
      call die( 'mm_assign: increase atsinres vector dimension (MAX_NUM_RES).' )
    atsinres(1:nroaa) = atxres(1:nroaa)

    deallocate( atnamea, atnu, resname, atxres )
    deallocate( attypea, nataa )

    ! Small verification of parameters.
    do iat = 1, na_qm
      if ( (qmattype(iat) == 'HO') .or. (qmattype(iat) == 'HW') ) cycle
      if ( (abs(Rm(iat)) > 0.0_dp)  .and. (abs(Em(iat)) > 0.0_dp) ) cycle

      write( 6, '(a,i6)' ) 'mm_assign: Null LJ parameter for QM atom ', iat
      write( 6, '(a,i6)' ) 'mm_assign: Rm = ', Rm(iat), ', Em = ', Em(iat)
    enddo

    do iat = 1, na_mm
      if ( (attype(iat) == 'HO') .or. (attype(iat) == 'HW') ) cycle
      if ( (abs(Rm(iat+na_qm)) > 0.0_dp)  .and. &
           (abs(Em(iat+na_qm)) > 0.0_dp) ) cycle

      write( 6, '(a,i6)' ) 'mm_assign: Null LJ parameter for MM atom ', iat
      write( 6, '(a,i6)' ) 'mm_assign: Rm = ', Rm(iat+na_qm), &
                                    ', Em = ', Em(iat+na_qm)
    enddo

    if( debug ) then
      write( 6, '(a)' ) 'mm_assign: Exiting routine successfully.'
      write( 6, '(a,70(1h=),/)' ) 'mm_assign: '
    endif
  end subroutine mm_assign

  subroutine check_ios_mm( ios, bname, iopt, iidx_i )
    use sys, only : die

    implicit none
    integer, intent(in) :: ios
      !! File I/O status when reading.
    integer, intent(in) :: bname
      !! Blockname to add to message.
    integer, intent(in), optional :: iopt
      !! Additional options for printing.
    integer, intent(in), optional :: iidx_i
      !! Index to print in message if necessary.

    character(len=255) :: msg
    character(len=5)   :: iidx, ios_s

    if ( ios == 0 ) return
    msg = 'mm_assign: ERROR:'

    ! Convert index to string in order to concat later.
    write( ios_s, '(I5)' ) ios
    if ( present( iidx_i ) ) write( iidx, '(I5)' ) iidx_i

    select case( bname )
    case (1) !! Classical atom information.
      msg = trim(msg)//' Problems reading residues block in amber.parm.'

      if ( present( iopt ) ) then
        if ( iopt == 1 ) msg = trim(msg)//' Number of residues not found.'
        if ( iopt == 2 ) msg = trim(msg)//' iaas = '//iidx//'.'
        if ( iopt == 3 ) msg = trim(msg)//' iat = ' //iidx//'.'
      endif

    case (2) !! Lennard-Jones
      msg = trim(msg)//' Problems reading Lennard-Jones block in amber.parm.'

      if ( present( iopt ) ) then
        if ( iopt == 1 ) msg = trim(msg)//' Number of LJ types not found.'
        if ( iopt == 2 ) msg = trim(msg)//' LJtype = '//iidx//'.'
      endif

    case (3) !! Connectivity and neighbours
      msg = trim(msg)//' Problems reading connectivity block in amber.parm.'

      if ( present( iopt ) ) then
        if ( iopt == 1 ) msg = trim(msg)//' Number of residues not found.'
        if ( iopt == 2 ) msg = trim(msg)//' Attempted to read resname'// &
                                 ' and bonds. ResID = '//iidx//'.'
        if ( iopt == 3 ) msg = trim(msg)//' Attempted to read ID'// &
                                 ' of neighbours. ResID = '//iidx//'.'
      endif

    case (4) !! Improper torsions
      msg = trim(msg)//' Problems reading impropers block in amber.parm.'

      if ( present( iopt ) ) then
        if ( iopt == 1 ) msg = trim(msg)//' Number of residues not found.'
        if ( iopt == 2 ) msg = trim(msg)//' Attempted to read resname'// &
                                 ' and imps per res. ResID = '//iidx//'.'
        if ( iopt == 3 ) msg = trim(msg)//' Attempted to read ID'// &
                                 ' of atoms in torsion. ResID = '//iidx//'.'
      endif

    case (5) !! Residue names and atoms.
      msg = trim(msg)//' Problems reading residues block in amber.parm.'
      if ( present( iopt ) ) then
        if ( iopt == 1 ) msg = trim(msg)//' Number of residues not found.'
        if ( iopt == 2 ) msg = trim(msg)//' Attempted to read resname'// &
                                 ' and number of atoms. ResID = '//iidx//'.'
        if ( iopt == 3 ) msg = trim(msg)//' Attempted to read atom'// &
                                 ' data. ResID = '//iidx//'.'
      endif

    case (6) !! Bond data.
      msg = trim(msg)//' Problems reading bonds block in amber.parm.'
      if ( present( iopt ) ) then
        if ( iopt == 1 ) msg = trim(msg)//' Number of bonds not found.'
        if ( iopt == 2 ) msg = trim(msg)//' Attempted to read bond data.'// &
                                 ' Bond ID = '//iidx//'.'
      endif

    case (7) !! Angle data.
      msg = trim(msg)//' Problems reading angles block in amber.parm.'
      if ( present( iopt ) ) then
        if ( iopt == 1 ) msg = trim(msg)//' Number of angles not found.'
        if ( iopt == 2 ) msg = trim(msg)//' Attempted to read angle data.'// &
                                 ' Angle ID = '//iidx//'.'
      endif

    case (8) !! Dihedral data.
      msg = trim(msg)//' Problems reading dihedrals block in amber.parm.'
      if ( present( iopt ) ) then
        if ( iopt == 1 ) msg = trim(msg)//' Number of dihedrals not found.'
        if ( iopt == 2 ) msg = trim(msg)//' Attempted to read dihedral '// &
                                 'angle data. Dihedral ID = '//iidx//'.'
      endif

    case (9) !! Improper torsions data.
      msg = trim(msg)//' Problems reading improper torsions block in amber.parm.'
      if ( present( iopt ) ) then
        if ( iopt == 1 ) msg = trim(msg)//' Number of torsions not found.'
        if ( iopt == 2 ) msg = trim(msg)//' Attempted to read improper '// &
                                 'torsions data. Improper ID = '//iidx//'.'
      endif

    case default
      msg = trim(msg)//' Problems with parameter file reading.'
    end select

    msg = trim(msg)//' IO status = '//ios_s//'.'

    call die( msg )
  end subroutine check_ios_mm

  subroutine paramats( nroaa, resname, atnamea, attypea, nataa, &
                       na_mm, atxres, pc, attype )
    !! Returns the atomic types and charges.
    use precision, only : dp
    use sys      , only : die

    implicit none
    integer         , intent(in)  :: na_mm
      !! Number of MM atoms.
    integer         , intent(in)  :: nroaa
      !! Number of classical residues.
    character(len=4), intent(in)  :: resname(nroaa)
      !! Classical residue names.
    character(len=4), intent(in)  :: atnamea(nroaa,100)
      !! Atom names within each residue.
    character(len=4), intent(out) :: attypea(nroaa,100)
      !! Atom types within each residue.
    integer         , intent(out) :: nataa(nroaa,100)
      !! Atoms index in each residue.
    integer         , intent(out) :: atxres(nroaa)
      !! Number of atoms in each residue.
    real(dp)        , intent(out) :: pc(na_mm)
      !! Atomic partial charges (classical).
    character(len=4), intent(out) :: attype(na_mm)
      !! MM atom types.

    real(dp)        , allocatable :: qaa(:,:), pcoord(:,:,:), pqaa(:,:)
    character(len=4), allocatable :: paanamea(:), patnamea(:,:), pattype(:,:)
    integer         , allocatable :: pnataa(:,:), patmas(:,:), patxres(:)

    character(len=12) :: option
    integer           :: n1, n2, n3, pnaas, ios, ui, iaas, iat, ires, jat
    logical           :: search

    external :: io_assign, io_close

    call io_assign( ui )
    open( unit = ui, file = "amber.parm" )

    ! Reads the number of atoms and the number of residues.
    ! The first line has both magnitudes; then reads each aminoacid and their
    ! variables.

    search = .true.
    ios    = 0
    do while ( search )
      read( ui, *, iostat = ios ) option
      if ( ios /= 0 ) call check_ios_mm( ios, 1 )

      if ( option == 'residues' ) then
        read( ui, *, iostat = ios ) pnaas
        if ( ios /= 0 ) call check_ios_mm( ios, 1, 1 )

        allocate( patnamea(pnaas,100), pcoord(pnaas,100,3), pattype(pnaas,100),&
                  patxres(pnaas)     , paanamea(pnaas)    , patmas(pnaas,100) ,&
                  pqaa(pnaas,100)    , pnataa(pnaas,100)  , qaa(nroaa,100) )

        do iaas = 1, pnaas
          read( ui, *, iostat = ios ) paanamea(iaas), patxres(iaas)
          if ( ios /= 0 ) call check_ios_mm( ios, 1, 2, iaas )

          do iat = 1, patxres(iaas)
            read( ui, *, iostat = ios ) patnamea(iaas,iat), pattype(iaas,iat),&
                                        n1, n2, n3, pnataa(iaas,iat),         &
                                        patmas(iaas,iat), pqaa(iaas,iat)
            if ( ios /= 0 ) call check_ios_mm( ios, 1, 3, iat )
          enddo
        enddo

        search = .false.
      endif
    enddo
    call io_close( ui )
    deallocate( pcoord, patmas )

    ! Assigns charges and atom types according to the AMBER forcefield.
    qaa = 100.0_dp
    do ires = 1, nroaa
    do iaas = 1, pnaas
      if ( .not. (resname(ires) == paanamea(iaas)) ) cycle

      do iat =1, patxres(iaas)
      do jat =1, patxres(iaas)
        if ( .not. (atnamea(ires,iat) == patnamea(iaas,jat)) ) cycle
        qaa(ires,iat)     = pqaa(iaas,jat)
        attypea(ires,iat) = pattype(iaas,jat)
        nataa(ires,iat)   = pnataa(iaas,jat)
      enddo
      enddo
    enddo
    enddo
    deallocate( patnamea, pattype, patxres, paanamea, pqaa, pnataa )

    jat = 1
    do ires = 1, nroaa
    do iat  = 1, atxres(ires)
      attype(jat) = attypea(ires,iat)
      jat         = jat +1
    enddo
    enddo

    jat = 1
    do ires = 1, nroaa
    do iat  = 1, atxres(ires)
      pc(jat) = qaa(ires,iat)

      if ( abs( pc(jat) - 100.0_dp ) < 1e-14_dp ) then
        write( 6, '(a,i5)' ) 'paramats: Wrong atom name:  ', jat
        call die( 'Paramats: Error while assigning charges.' )
      endif

      jat = jat +1
    enddo
    enddo

    deallocate( qaa )
  end subroutine paramats

  subroutine read_lj( nroaa, attypea, na_qm, na_mm, Em, Rm, qmattype, atxres )
    !! Assigns Lennard-Jones parameters according to the atom types in attypea.
    use precision, only : dp
    use sys      , only : die

    implicit none
    integer         , intent(in)  :: na_mm
      !! Number of MM atoms.
    integer         , intent(in)  :: na_qm
      !! Number of QM atoms.
    integer         , intent(in)  :: nroaa
      !! Number of classical residues.
    character(len=4), intent(in)  :: qmattype(na_qm)
      !! QM atom types.
    character(len=4), intent(in)  :: attypea(nroaa,100)
      !! Atom types within each residue.
    integer         , intent(in)  :: atxres(nroaa)
      !! Number of atoms in each residue.
    real(dp)        , intent(out) :: Em(na_mm+na_qm)
      !! Lennard-Jones epsilon.
    real(dp)        , intent(out) :: Rm(na_mm+na_qm)
      !! Lennard-Jones Rmin.

    integer           :: ilj, nlj, ui, ios, ires, iat
    logical           :: search
    character(len=12) :: option

    real(dp)        , allocatable :: pEm(:), pRm(:), Ema(:,:), Rma(:,:)
    character(len=4), allocatable :: ljtype(:)

    external :: io_assign, io_close

    Rm(:) = 0.0_dp
    Em(:) = 0.0_dp

    allocate( pEm(MAX_TYPE_RES), pRm(MAX_TYPE_RES), ljtype(MAX_TYPE_RES) )

    ! Opens and reads the parameter files.
    call io_assign( ui )
    open( unit = ui, file = "amber.parm" )

    ios    = 0
    search = .true.
    do while ( search )
      read ( ui, *, iostat = ios ) option
      call check_ios_mm( ios, 2 )

      if ( option == 'ljs' ) then
        read( ui, *, iostat = ios ) nlj
        call check_ios_mm( ios, 2, 1 )

        if ( nlj > MAX_TYPE_RES ) &
          call die( 'lj: LJ parameters must not exeed MAX_TYPE_RES.' )
        do ilj = 1, nlj
          read ( ui, *, iostat = ios ) ljtype(ilj), pRm(ilj), pEm(ilj)
          call check_ios_mm( ios, 2, 2, ilj )
        enddo

        search = .false.
      endif
    enddo
    call io_close( ui )

    ! Converts units.
    do ilj = 1, nlj
      pRm(ilj) = 2.0_dp*pRm(ilj) / ( 0.529177_dp * (2.0_dp)**(1.0_dp/6.0_dp) ) 
      pEm(ilj) = pEm(ilj) / 627.5108_dp
    enddo

    ! Assigns the LJ parameters corresponding to MM atom types.
    allocate( Ema(nroaa,100), Rma(nroaa,100) )

    do ires = 1, nroaa
    do iat  = 1, atxres(ires)
      if ( (attypea(ires,iat) == 'C' ) .or.  &
           (attypea(ires,iat) == 'CA') .or. (attypea(ires,iat) == 'CM') .or. &
           (attypea(ires,iat) == 'CC') .or. (attypea(ires,iat) == 'CV') .or. &
           (attypea(ires,iat) == 'CW') .or. (attypea(ires,iat) == 'CR') .or. &
           (attypea(ires,iat) == 'CB') .or. (attypea(ires,iat) == 'C*') .or. &
           (attypea(ires,iat) == 'CN') .or. (attypea(ires,iat) == 'CK') .or. &
           (attypea(ires,iat) == 'CQ') .or. (attypea(ires,iat) == 'CX') .or. &
           (attypea(ires,iat) == 'CY') .or. (attypea(ires,iat) == 'CD') ) then

        do ilj = 1, nlj
          if ( .not. (ljtype(ilj) == 'C') ) cycle
          Rma(ires,iat) = pRm(ilj)
          Ema(ires,iat) = pEm(ilj)
        enddo

      elseif ((attypea(ires,iat) == 'N' ) .or. (attypea(ires,iat) == 'NA') .or.&
              (attypea(ires,iat) == 'NB') .or. (attypea(ires,iat) == 'NC') .or.&
              (attypea(ires,iat) == 'N*') .or. (attypea(ires,iat) == 'N2') .or.&
              (attypea(ires,iat) == 'NO') .or. (attypea(ires,iat) == 'NP')) then

        do ilj = 1, nlj
          if ( .not. (ljtype(ilj) == 'N') ) cycle
          Rma(ires,iat) = pRm(ilj)
          Ema(ires,iat) = pEm(ilj)
        enddo

      else
        do ilj = 1, nlj
        if ( attypea(ires,iat) == ljtype(ilj) ) then
            Rma(ires,iat) = pRm(ilj)
            Ema(ires,iat) = pEm(ilj)
        endif
        enddo

      endif
    enddo
    enddo

    ! Collapses MM parameters to each MM atom.
    ilj = na_qm +1
    do ires = 1, nroaa
    do iat  = 1, atxres(ires)
      Em(ilj) = Ema(ires,iat)
      Rm(ilj) = Rma(ires,iat)
      ilj = ilj +1
    enddo
    enddo
    deallocate( Ema, Rma )

    ! Assigns the LJ parameters corresponding to QM atom types.
    do iat  = 1, na_qm
      if ( (qmattype(iat) == 'C' ) .or.  &
           (qmattype(iat) == 'CA') .or. (qmattype(iat) == 'CM') .or. &
           (qmattype(iat) == 'CC') .or. (qmattype(iat) == 'CV') .or. &
           (qmattype(iat) == 'CW') .or. (qmattype(iat) == 'CR') .or. &
           (qmattype(iat) == 'CB') .or. (qmattype(iat) == 'C*') .or. &
           (qmattype(iat) == 'CN') .or. (qmattype(iat) == 'CK') .or. &
           (qmattype(iat) == 'CQ') .or. (qmattype(iat) == 'CX') .or. &
           (qmattype(iat) == 'CY') .or. (qmattype(iat) == 'CD') ) then

        do ilj = 1, nlj
          if ( .not. (ljtype(ilj) == 'C') ) cycle
          Rm(iat) = pRm(ilj)
          Em(iat) = pEm(ilj)
        enddo

      elseif ( (qmattype(iat) == 'N' ) .or. (qmattype(iat) == 'NA') &
          .or. (qmattype(iat) == 'NB') .or. (qmattype(iat) == 'NC') &
          .or. (qmattype(iat) == 'N*') .or. (qmattype(iat) == 'N2') &
          .or. (qmattype(iat) == 'NO') .or. (qmattype(iat) == 'NP') &
          ) then

        do ilj = 1, nlj
          if ( .not. (ljtype(ilj) == 'N') ) cycle
          Rm(iat) = pRm(ilj)
          Em(iat) = pEm(ilj)
        enddo

      else
        do ilj = 1, nlj
        if ( qmattype(iat) == ljtype(ilj) ) then
          Rm(iat) = pRm(ilj)
          Em(iat) = pEm(ilj)
        endif
        enddo

      endif
    enddo

    deallocate( pEm, pRm, ljtype )
  end subroutine read_lj

  subroutine amber_ng1( na_mm, nataa, nroaa, atxres, atnu, resname, ng1, &
                        atnamea, ncon, con, rmm, mmcell, lattice_type )
    !! Assigns first neighbours accodring to connectivity.
    use functions, only : dist_v2
    use precision, only : dp
    use qmmm_pbc , only : pbc_displ_vector, reccel

    implicit none
    integer         , intent(in)  :: na_mm
      !! Number of MM atoms.
    integer         , intent(in)  :: nroaa
      !! Number of classical residues.
    integer         , intent(in)  :: nataa(nroaa,100)
      !! Atom index within each residue.
    integer         , intent(in)  :: atxres(nroaa)
      !! Number of atoms in each residue.
    integer         , intent(in)  :: atnu(nroaa,100)
      !! Global index of an atom within a given residue.
    character(len=4), intent(in)  :: resname(nroaa)
      !! Atom types within each residue.
    character(len=4), intent(in)  :: atnamea(nroaa,100)
      !! Atom types within each residue.
    integer         , intent(in)  :: ncon
      !! Total number of connectivity entries.
    integer         , intent(in)  :: con(2,ncon)
      !! Indices for each pair of atoms in a given connection.
    real(dp)        , intent(in)  :: rmm(3,na_mm)
      !! Cartesian atomic positions.
    real(dp)        , intent(in)  :: mmcell(3,3)
      !! Unit cell vectors.
    character(len=1), intent(in)  :: lattice_type
      !! Type of lattice used.
    integer         , intent(out) :: ng1(na_mm,6)
      !! First neighbour indices for each MM atom.

    character(len=12) :: option
    character(len=1)  :: c1
    character(len=2)  :: c2
    character(len=4)  :: c4
    integer           :: ui, ires, jres, iat, jat, ios, ineig, icon, nresid
    logical           :: search
    real(dp)          :: kcell(3,3), dx, dy, dz, rij

    character(len=4), allocatable :: presname(:)
    integer         , allocatable :: png1(:,:,:), bondxres(:)

    external :: io_assign, io_close

    call io_assign( ui )
    open( unit = ui, file = "amber.parm" )

    ios    = 0
    search = .true.
    do while ( search )
      read ( ui, *, iostat = ios ) option
      call check_ios_mm( ios, 3 )

      if ( option == 'connectivity' ) then
        read( ui, *, iostat = ios ) nresid
        call check_ios_mm( ios, 1 )

        allocate( presname(nresid), bondxres(nresid), png1(nresid,100,2) )

        do ires = 1, nresid
          read( ui, *, iostat = ios ) presname(ires), bondxres(ires)
          call check_ios_mm( ios, 2, ires )

          do iat = 1, bondxres(ires)
            read( ui, *, iostat = ios ) png1(ires,iat,1), png1(ires,iat,2)
            call check_ios_mm( ios, 3, ires )
          enddo
        enddo

        search = .false.
      endif
    enddo
    call io_close(ui)

    ! Assigns first neighbours for each atom.
    do ires = 1, nroaa
    do jres = 1, nresid
      if ( .not. (resname(ires) == presname(jres)) ) cycle

      do iat = 1, atxres(ires)
        ineig = 1

        do icon = 1, bondxres(jres)
          if ( nataa(ires,iat) == png1(jres,icon,1) ) then
            do jat = 1, atxres(ires)
              if ( .not. (nataa(ires,jat) == png1(jres,icon,2)) ) cycle
              ng1(atnu(ires,iat),ineig) = atnu(ires,jat)
              ineig = ineig +1
            enddo

          elseif ( nataa(ires,iat) == png1(jres,icon,2) ) then
            do jat = 1, atxres(ires)
              if ( .not. (nataa(ires,jat) == png1(jres,icon,1)) ) cycle
              ng1(atnu(ires,iat),ineig) = atnu(ires,jat)
              ineig = ineig +1
            enddo
          endif
        enddo ! Connections for atom iat.
      enddo   ! Atom iat in a residue.
    enddo
    enddo
    deallocate( presname, bondxres, png1 )

    ! Neighbour assignments for graphene slabs.
    call reccel( 3, mmcell, kcell, 0 )

    do ires = 1, nroaa
      if ( .not. ((resname(ires) == 'GRAP') .or. (resname(ires) == 'GRAH')) )&
        cycle
      ineig = 1
      do jres = 1, nroaa
        if ( ires == jres ) cycle
        if ( .not. ((resname(jres) == 'GRAP') .or. (resname(jres) == 'GRAH')) )&
          cycle

        dx = rmm(1,atnu(ires,1)) - rmm(1,atnu(jres,1))
        dy = rmm(2,atnu(ires,1)) - rmm(2,atnu(jres,1))
        dz = rmm(3,atnu(ires,1)) - rmm(3,atnu(jres,1))
        call pbc_displ_vector( lattice_type, mmcell, kcell, dx, dy, dz )

        rij = dist_v2( dx, dy, dz )

        ! dx is now a dummy array.
        if ( resname(ires) == resname(jres) ) then
          dx = 1.7_dp / 0.529177_dp
        else
          dx = 1.4_dp / 0.529177_dp
        endif

        if ( rij < dx ) then
          ng1(atnu(ires,1),ineig) = atnu(jres,1)
          ineig = ineig +1
        endif
      enddo
    enddo

    ! Finds neighbours of 2 contiguous residues.
    do ires = 2,nroaa
      c4 = resname(ires)
      c1 = c4(1:1)

      if ( c1 == 'N' ) then ! Might be a terminal N.
        if ( .not. ((c4 == 'NME') .or. (c4 == 'NALB')) ) cycle
      endif

      do iat = 1, atxres(ires)
        if ( atnamea(ires,iat) /= 'N' ) cycle

        do jat = 1, atxres(ires-1)
          if ( atnamea(ires-1,jat) /= 'C') cycle
          ng1(atnu(ires,iat),3)   = atnu(ires-1,jat)
          ng1(atnu(ires-1,jat),3) = atnu(ires,iat)
        enddo
      enddo
    enddo

    ! Find neighbours between two consecutive nucleotides.
    do ires = 2, nroaa
      c4 = resname(ires)
      c1 = c4(3:3)
      if ( c1 == '5' ) cycle

      do jres = 1, atxres(ires)
        if ( atnamea(ires,jres) /= 'P' ) cycle

        do iat = 1, atxres(ires-1)
          c4 = atnamea(ires-1,iat)
          c2 = c4(1:2)
          if ( c2 /= 'O3' ) cycle

          ng1(atnu(ires,jres),4)  = atnu(ires-1,iat)
          ng1(atnu(ires-1,iat),2) = atnu(ires,jres)
        enddo
      enddo
    enddo

    ! Assings any additional connections forced in the input.
    do icon = 1, ncon
    do iat  = 1, 6
      if ( ng1(con(1,icon),iat) /= 0 ) cycle
      search = .false.

      ng1(con(1,icon),iat) = con(2,icon)

      do ineig = 1, 6

        ! Here we use search as a dummy variable to control flow.
        if ( ng1(con(2,icon),ineig) /= 0 ) cycle
        ng1(con(2,icon),ineig) = con(1,icon)
        search = .true.
        exit
      enddo
      if ( search ) exit ! Go to the next icon.
    enddo
    enddo
  end subroutine amber_ng1

  subroutine bon_ang_dih_imp( na_mm, ng1, atange, atangm, atdihe, atdihm,   &
                              bondxat, angexat, angmxat, dihexat, dihmxat,  &
                              atnamea, nroaa, atxres, atnu, resname, atimp, &
                              impxat )
    ! Assigns all connectivity magnitudes: bonds, angles, dihedrals and
    ! improper torsions. Also, reads impropers data.
    use precision, only : dp
    use sys      , only : die

    implicit none
    integer         , intent(in)  :: na_mm
      !! Number of MM atoms.
    integer         , intent(in)  :: nroaa
      !! Number of classical residues.
    integer         , intent(in)  :: atxres(nroaa)
      !! Number of atoms in each residue.
    integer         , intent(in)  :: atnu(nroaa,100)
      !! Global index of an atom within a given residue.
    character(len=4), intent(in)  :: resname(nroaa)
      !! Atom types within each residue.
    character(len=4), intent(in)  :: atnamea(nroaa,100)
      !! Atom types within each residue.
    integer         , intent(in)  :: ng1(na_mm,6)
      !! First neighbour indices for each MM atom.
    integer         , intent(out) :: bondxat(na_mm)
      !! Number of bonds involving a given atom.
    integer         , intent(out)   :: angexat(na_mm)
      !! Number of angles involving a given atom in the endpoints.
    integer         , intent(out)   :: angmxat(na_mm)
      !! Number of angles involving a given atom in the middle.
    integer         , intent(out)   :: atange(na_mm,25,2)
      !! For the angles in angexat, the indices of neighbours involved.
    integer         , intent(out)   :: atangm(na_mm,25,2)
      !! For the angles in angmxat, the indices of neighbours involved.
    integer         , intent(out)   :: dihexat(na_mm)
      !! Number of dihedrals involving a given atom in the endpoints.
    integer         , intent(out)   :: dihmxat(na_mm)
      !! Number of dihedrals involving a given atom in the middle.
    integer         , intent(out)   :: atdihe(na_mm,100,3)
      !! For the angles in dihexat, the indices of neighbours involved.
    integer         , intent(out)   :: atdihm(na_mm,100,3)
      !! For the angles in dihmxat, the indices of neighbours involved.
    integer         , intent(out)   :: impxat(na_mm)
      !! Number of improper angles involving a given atom in the endpoints.
    integer         , intent(out)   :: atimp(na_mm,25,4)
      !! For the improper angles in impxat, the indices of neighbours involved.

    integer           :: iat, jat, kat, lat, mat, ires, jres, iimp, jimp, &
                         ios, ui, nresid, ntot, impmax
    logical           :: search
    character(len=10) :: option

    character(len=4), allocatable :: impatnamea(:,:,:), presname(:)
    integer         , allocatable :: pimpxres(:), impnum(:,:)

    external :: io_assign, io_close

    ! Reads and calculates improper torsions.
    call io_assign( ui )
    open( unit = ui, file = "amber.parm" )

    ios    = 0
    search = .true.
    do while ( search )
      read ( ui, *, iostat = ios ) option
      call check_ios_mm( ios, 4 )

      if ( option == 'impropers' ) then
        read( ui, *, iostat = ios ) nresid
        call check_ios_mm( ios, 4, 1 )

        allocate( presname(nresid), pimpxres(nresid), &
                  impatnamea(nresid,50,4) )

        do ires = 1, nresid
          read( ui, *, iostat = ios ) presname(ires), pimpxres(ires)
          call check_ios_mm( ios, 4, 2, ires )

          do iat = 1, pimpxres(ires)
            read( ui, *, iostat = ios ) impatnamea(ires,iat,1), &
                                        impatnamea(ires,iat,2), &
                                        impatnamea(ires,iat,3), &
                                        impatnamea(ires,iat,4)
            call check_ios_mm( ios, 4, 3, ires )
          enddo
        enddo

        search = .false.
      endif
    enddo
    call io_close( ui )

    ! Assigns impropers according to atoms, starting from the
    ! second residue.
    impmax = nroaa * 25

    allocate( impnum(impmax,4) )
    impnum = 0
    ntot   = 1

    do ires = 1, nresid
    do jres = 1, nroaa
      if ( presname(ires) /= resname(jres) ) cycle
      do iimp = 1, pimpxres(ires)
        do iat = 1, 4

        if ( (impatnamea(ires,iimp,iat) == '+M') .and. (jres /= nroaa) ) then
          do jat = 1, atxres(jres+1)
            if ( atnamea(jres+1,jat) /= 'N') cycle
            impnum(ntot,iat) = atnu(jres+1,jat)
          enddo

        elseif ( (impatnamea(ires,iimp,iat) == '-M') .and. (jres /= 1) ) then
          do jat = 1,atxres(jres-1)
            if ( atnamea(jres-1,jat) /= 'C') cycle
            impnum(ntot,iat) = atnu(jres-1,jat)
          enddo

        else
          do jat = 1, atxres(jres)
            if ( atnamea(jres,jat) /= impatnamea(ires,iimp,iat) ) cycle
            impnum(ntot,iat) = atnu(jres,jat)
          enddo
        endif
        enddo

        ntot = ntot +1
        if ( ntot >= impmax ) &
          call die( 'mm_assign: increase the size of the improper matrix.' )
      enddo
    enddo
    enddo

    do iimp = 1, ntot
    do iat  = 1, 4
      if (impnum(iimp,iat) /= 0) cycle
      impnum(iimp,:) = 0
    enddo
    enddo

    do iat = 1, na_mm
      jimp = 0
      do iimp = 1, ntot
        if ( (impnum(iimp,1) == iat) .or. (impnum(iimp,2) == iat) .or. &
             (impnum(iimp,3) == iat) .or. (impnum(iimp,4) == iat) ) then
          jimp = jimp +1
          atimp(iat,jimp,:) = impnum(iimp,:)
        endif
      enddo
      impxat(iat) = jimp
    enddo

    deallocate( impnum, presname, pimpxres, impatnamea )
    ! Finished with impropers.

    ! Now we use connectivity data for bonds, angles and dihedrals.
    ! Here we assign bonds.
    do iat = 1, na_mm
      bondxat(iat) = 0
      do jat = 1, 6
        if ( ng1(iat,jat) /= 0 ) bondxat(iat) = bondxat(iat) +1
      enddo
    enddo

    ! Searches for angles with atom iat in the extremes.
    do iat = 1, na_mm
      ntot = 1
      do jat = 1, bondxat(iat)
        kat = ng1(iat,jat)

        do lat = 1, bondxat(kat)
          if ( ng1(kat,lat) == iat ) cycle

          atange(iat,ntot,1) = ng1(iat,jat)
          atange(iat,ntot,2) = ng1(kat,lat)
          ntot = ntot +1
        enddo
      enddo

      angexat(iat) = ntot -1
    enddo

    ! Searches for angles with atom iat in the middle.
    do iat = 1, na_mm
      ntot = 1

      do jat = 1, bondxat(iat)
      do kat = 1, bondxat(iat)
        if ( ng1(iat,kat) <= ng1(iat,jat) ) cycle

        atangm(iat,ntot,1) = ng1(iat,jat)
        atangm(iat,ntot,2) = ng1(iat,kat)
        ntot = ntot +1
      enddo
      enddo

      angmxat(iat) = ntot -1
    enddo

    ! Searches for dihedrals with atom iat in the extremes.
    do iat = 1, na_mm
      ntot = 1

      do jat = 1, angexat(iat)
        kat = atange(iat,jat,2)

        do lat = 1, bondxat(kat)
          mat = ng1(kat,lat)

          if ( mat /= atange(iat,jat,1) ) then
            atdihe(iat,ntot,1) = atange(iat,jat,1)
            atdihe(iat,ntot,2) = kat
            atdihe(iat,ntot,3) = mat
            ntot = ntot +1
          endif
        enddo
      enddo
      dihexat(iat) = ntot -1
    enddo

    ! Searches for dihedrals with atom iat in the middle, starting
    ! from angles that have that same atom in the extreme.
    do iat = 1, na_mm
      ntot = 1

      do jat = 1, angexat(iat)
        kat = atange(iat,jat,1)

        do lat = 1, bondxat(iat)
          mat = ng1(iat,lat)
          if ( mat /= kat ) then
            atdihm(iat,ntot,1) = mat
            atdihm(iat,ntot,2) = kat
            atdihm(iat,ntot,3) = atange(iat,jat,2)
            ntot = ntot +1
          endif
        enddo
      enddo

      dihmxat(iat) = ntot -1
    enddo
  end subroutine bon_ang_dih_imp

  subroutine atxaa( ntres, aanamea, atomsxaa )
    !! Returns how many atoms are in each residue, and eaech residue's name.
    use precision, only : dp
    use sys      , only : die

    implicit none
    integer         , intent(out) :: ntres
      !! Number of residue types.
    character(len=4), intent(out) :: aanamea(MAX_TYPE_RES)
      !! Name of each residue.
    integer         , intent(out) :: atomsxaa(MAX_TYPE_RES)
      !! Number of atoms per residue.

    character(len=4)  :: dummychar
    character(len=10) :: option
    integer           :: ui, ires, iat, ios
    logical           :: search

    external :: io_assign, io_close

    call io_assign( ui )
    open( unit = ui, file = "amber.parm" )

    ios    = 0
    search = .true.
    do while ( search )
      read ( ui, *, iostat = ios ) option
      call check_ios_mm( ios, 5 )

      if ( option == 'residues' ) then
        read( ui, *, iostat = ios ) ntres
        call check_ios_mm( ios, 5 , 1 )

        if ( ntres > MAX_TYPE_RES ) &
          call die( 'mm_assign: The number of different residues must'//&
                    ' not exceed MAX_TYPE_RES.' )

        do ires = 1, ntres
          read( ui, *, iostat = ios ) aanamea(ires),atomsxaa(ires)
          call check_ios_mm( ios, 5 , 2, ires )

          if ( atomsxaa(ires) > 100 ) &
            call die( 'mm_assign: Number of atoms in a given residue must'//&
                      ' not exceed 100.' )

          ! Basically skips over the next nAtoms lines.
          do iat = 1, atomsxaa(ires)
            read( ui, *, iostat = ios ) dummychar
            call check_ios_mm( ios, 5 , 3, ires )
          enddo
        enddo

        search = .false.
        exit
      endif
    enddo

    call io_close( ui )
  end subroutine atxaa

  subroutine amber_union_parms( nbond, kbond, bondeq, bondtype, nangle,     &
                                kangle, angleeq, angletype, ndihe, kdihe,   &
                                diheeq, dihetype, multidihe, perdihe, nimp, &
                                kimp, impeq, imptype, multiimp, perimp, nparm )
    !! Reads the number, constants and other parameters for bonds,
    !! angles, dihedrals and improper torsions.
    use precision, only : dp
    use sys      , only : die

    implicit none
    integer          , intent(in)    :: nparm
      !! Maximum number of any kind of parameter.

    integer          , intent(out)   :: nbond
      !! Total number of bonds.
    real(dp)         , intent(out)   :: kbond(nparm)
      !! List of bond constants.
    real(dp)         , intent(out)   :: bondeq(nparm)
      !! List of bond equilibrium distances.
    character(len=5) , intent(out)   :: bondtype(nparm)
      !! Bond types.

    integer          , intent(out)   :: nangle
      !! Total number of angles.
    real(dp)         , intent(out)   :: kangle(nparm)
      !! List of angle constants.
    real(dp)         , intent(out)   :: angleeq(nparm)
      !! List of angle equilibrium positions.
    character(len=8) , intent(out)   :: angletype(nparm)
      !! Angle types.

    integer          , intent(out)   :: ndihe
      !! Total number of dihedral angles.
    real(dp)         , intent(out)   :: kdihe(nparm)
      !! List of dihedral angle constants.
    real(dp)         , intent(out)   :: diheeq(nparm)
      !! List of dihedral angle equilibrium positions.
    character(len=11), intent(out)   :: dihetype(nparm)
      !! Dihedral angle types.
    integer          , intent(out)   :: multidihe(nparm)
      !! Dihedral angle multiplicity (in 360°).
    real(dp)         , intent(out)   :: perdihe(nparm)
      !! Dihedral angle periodicity (angle value).

    integer          , intent(out)   :: nimp
      !! Total number of improper angles.
    real(dp)         , intent(out)   :: kimp(nparm)
      !! List of improper angle constants.
    real(dp)         , intent(out)   :: impeq(nparm)
      !! List of improper angle equilibrium positions.
    character(len=11), intent(out)   :: imptype(nparm)
      !! Improper angle types.
    integer          , intent(out)   :: multiimp(nparm)
      !! Improper angle multiplicity (in 360°).
    real(dp)         , intent(out)   :: perimp(nparm)
      !! Improper angle periodicity (angle value).

    character(len=12) :: option
    integer           :: ui, ios, ipar
    logical           :: search

    external :: io_assign, io_close

    ! Bond data.
    call io_assign( ui )
    open( unit = ui, file = "amber.parm", status = 'old', action = 'read' )

    ios    = 0
    search = .true.
    do while ( search )
      read ( ui, *, iostat = ios ) option
      call check_ios_mm( ios, 6 )

      if ( option == 'bonds' ) then
        read( ui, *, iostat = ios ) nbond
        call check_ios_mm( ios, 6, 1 )

        if ( nbond > nparm ) &
          call die( 'mm_assign: Number of bonds greater than Nparm.' )

        do ipar = 1, nbond
          read( ui, fmt = '(A5,2x,F5.1,4x,F6.4)', iostat = ios ) &
            bondtype(ipar), kbond(ipar), bondeq(ipar)
          call check_ios_mm( ios, 6, 2, ipar )
        enddo

        search = .false.
        exit
      endif
    enddo

    ! Angle data
    rewind ( ui )
    search = .true.
    ios    = 0

    angletype(:) = ""
    kangle(:)    = 0.0_dp
    angleeq(:)   = 0.0_dp

    do while ( search )
      read ( ui, *, iostat = ios ) option
      call check_ios_mm( ios, 7 )

      if ( option == 'angles' ) then
        read( ui, *, iostat = ios ) nangle
        call check_ios_mm( ios, 7, 1 )

        if ( nangle > nparm ) &
          call die( 'mm_assign: Number of angles greater than Nparm.' )

        do ipar = 1, nangle
          read( ui, fmt = '(A8,3x,F5.1,6x,F6.2)', iostat = ios ) &
            angletype(ipar), kangle(ipar), angleeq(ipar)

          call check_ios_mm( ios, 7, 2, ipar )
        enddo

        search = .false.
        exit
      endif
    enddo

    ! Dihedrals
    rewind( ui )
    search = .true.
    ios    = 0

    do while ( search )
      read ( ui, *, iostat = ios ) option
      call check_ios_mm( ios, 8 )

      if ( option == 'dihes' ) then
        read( ui, *, iostat = ios ) ndihe
        call check_ios_mm( ios, 8, 1 )

        if ( ndihe > nparm ) &
          call die( 'mm_assign: Number of dihedrals greater than Nparm.' )

        do ipar = 1, ndihe
          read( ui, fmt = '(A11,3x,I1,3x,F6.3,7x,F7.3,10x,F6.3)',         &
                iostat = ios ) dihetype(ipar), multidihe(ipar), kdihe(ipar), &
                               diheeq(ipar)  , perdihe(ipar)

          call check_ios_mm( ios, 8, 2, ipar )
        enddo
        search = .false.
        exit
      endif
    enddo

    ! Improper torsions.
    rewind( ui )
    search = .true.
    ios    = 0

    do while ( search )
      read ( ui, *, iostat = ios ) option
      call check_ios_mm( ios, 9 )

      if ( option == 'imps' ) then
        read( ui, *, iostat = ios ) nimp
        call check_ios_mm( ios, 9, 1 )

        if ( nimp > nparm ) &
          call die( 'mm_assign: Number of improper torsions greater'//&
                    ' than Nparm.' )

        do ipar = 1, nimp
          read( ui, fmt = '(A11,3x,I1,3x,F6.3,7x,F7.3,10x,F6.3)',      &
                iostat = ios ) imptype(ipar), multiimp(ipar), kimp(ipar), &
                               impeq(ipar)  , perimp(ipar)
          call check_ios_mm( ios, 9, 1, ipar )
        enddo

        search = .false.
        exit
      endif
    enddo
    call io_close( ui )
  end subroutine amber_union_parms
end module mm_assign_m
